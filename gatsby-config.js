require("dotenv").config({
    path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
    siteMetadata: {
        title: `yload-global-landing`,
        siteUrl: `https://global.yload.eu`
    },
    plugins: [
        "gatsby-plugin-sass",
        "gatsby-plugin-image",
        "gatsby-plugin-react-helmet",
        "gatsby-plugin-sitemap",
        "gatsby-plugin-sharp",
        "gatsby-transformer-sharp",
        {
            resolve: 'gatsby-plugin-manifest',
            options: {
                "icon": "src/images/icon.png"
            }
        },
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                "name": "images",
                "path": "./src/images/"
            },
            __key: "images"
        },
        {
            resolve: `gatsby-plugin-intl`,
            options: {
                path: `${__dirname}/src/locales`,
                languages: [`en`, `ro`, `de`, `pl`],
                defaultLanguage: `en`,
                redirect: false,
            },
        },
        {
            resolve: `gatsby-source-contentful`,
            options: {
                spaceId: process.env.CONTENTFUL_SPACE_ID,
                accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
            },
        },
        {
            resolve: `gatsby-transformer-yaml`,
            options: {
                // Conditionally set the typeName so that we both use a lowercased and capitalized type name
                typeName: ({node}) => {
                    const name = node.sourceInstanceName
                    if (name === `jobs`) {
                        return `job`
                    }
                    return name
                },
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                path: `${__dirname}/src/jobs`,
                name: `jobs`,
            },
        },
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                name: `Yload Global`,
                short_name: `Yload Global`,
                start_url: `/`,
                lang: `en`,
                background_color: `#eff0f2`,
                theme_color: `#eff0f2`,
                display: `standalone`,
                icon: `src/images/icon.png`,
                cache_busting_mode: 'none',
                localize: [
                    {
                        start_url: `/ro/`,
                        lang: `ro`,
                        name: `Yload Global`,
                        short_name: `Yload Global`,
                    },
                ],
            },
        },
    ]
};
