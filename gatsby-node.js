const path = require(`path`)
// Log out information after a build is done
exports.onPostBuild = ({reporter}) => {
    reporter.info(`Your Blog site has been built!`)
}
// Create blog pages dynamically
exports.createPages = async ({graphql, actions}) => {
    const {createPage} = actions;
    const blogPostTemplate = path.resolve(`src/templates/BlogPost.jsx`);
    const newsTemplate = path.resolve(`src/templates/NewsTemplate.jsx`);
    const result = await graphql(`
    query {
      allContentfulBlogPage{
        edges {
          node {
              slug
              title
              heroImage {
                gatsbyImage(placeholder: BLURRED, width: 3000, quality: 100)
                file {
                  url
                }
              }
              description {
                description
              }
              content {
                raw
                references {
                   ... on ContentfulAsset {
                contentful_id
                __typename
                file {
                    url
                }
              }
                  gatsbyImage(placeholder: BLURRED, width: 3000, quality: 100)
                }
              }
              author {
                avatar {
                  file {
                    url
                  }
                }
                name
                role
              }
              readingTime
              publishDate(formatString: "DD MMM YYYY")
              tags
              relatedArticles {
                title
                heroImage {
                  file {
                    url
                  }
                }
                description {
                  description
                }
                slug
              }
          }
        }
      }
    }
  `)
    const resultNews = await graphql(`
        query {
          allContentfulNews {
            edges {
              node {
                id
                date(formatString: "DD MMM YYYY")
                slug
                description {
                  description
                }
                content {
                  raw
                }
                title
              }
            }
          }
        }
    `)
    result.data?.allContentfulBlogPage.edges.forEach(edge => {
        createPage({
            path: `${edge.node.slug}`,
            component: blogPostTemplate,
            context: {
                slug: edge.node.slug,
            },
        })
    });
    resultNews.data?.allContentfulNews.edges.forEach(edge => {
        createPage({
            path: `${edge.node.slug}`,
            component: newsTemplate,
            context: {
                slug: edge.node.slug,
            },
        })
    })
}