import React from 'react'

export const EmailContext = React.createContext({
    email: '',
    setEmail: () => {},
})

const EmailProvider = props => {
    const [email, setEmail] = React.useState('')
    const value = { email, setEmail }

    return (
        <EmailContext.Provider value={value}>
            {props.children}
        </EmailContext.Provider>
    )
}

export default ({ element }) => <EmailProvider>{element}</EmailProvider>