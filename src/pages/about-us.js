import React, {useEffect} from "react";
import useScroll from "../hooks/useScroll";
import Navbar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";
import AboutUs from "../components/AboutUs/AboutUs";
import SEOComponent from "../components/SEOComponent/SEOComponent";

const AboutUsPage = () => {
    const {bodyOffset, setScrollDirection} = useScroll();

    useEffect(() => {
        if(bodyOffset.top === 0) {
            setScrollDirection('down')
        }
    })

    return (
        <div className="layout_container">
            <SEOComponent/>
            <Navbar navbarClass="partner_navbar"/>
            <AboutUs/>
            <Footer isDarkFooter={true}/>
        </div>
    )
}

export default AboutUsPage;
