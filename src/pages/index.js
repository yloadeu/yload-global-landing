import * as React from "react"
import SEOComponent from "../components/SEOComponent/SEOComponent";
import Navbar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";
import HomePage from "../components/HomePage/HomePage";

const IndexPage = () => {
    return (
        <div className="layout_container">
            <SEOComponent/>
            <Navbar/>
            <HomePage/>
            <Footer/>
        </div>
    )
}

export default IndexPage
