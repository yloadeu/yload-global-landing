import React, {useEffect} from 'react';
import useScroll from "../hooks/useScroll";
import SEOComponent from "../components/SEOComponent/SEOComponent";
import Navbar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";
import Broker from "../components/Broker/Broker";

const BrokerPartnership = () => {
    const {bodyOffset, setScrollDirection} = useScroll();

    useEffect(() => {
        if (bodyOffset.top === 0) {
            setScrollDirection('down')
        }
    })

    return (
        <div className="layout_container">
            <SEOComponent/>
            <Navbar/>
            <Broker/>
            <Footer/>
        </div>
    )

}

export default BrokerPartnership;