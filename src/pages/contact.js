import * as React from 'react';
import Footer from "../components/Footer/Footer";
import ContactForm from "../components/Contact/ContactForm";
import Navbar from "../components/Navbar/Navbar";
import useScroll from "../hooks/useScroll";
import {useEffect} from "react";
import SEOComponent from "../components/SEOComponent/SEOComponent";

const ContactPage = () => {
    const {bodyOffset, setScrollDirection} = useScroll();

    useEffect(() => {
        if (bodyOffset.top === 0) {
            setScrollDirection('down');
        }
    })

    return (
        <div className="layout_container">
            <SEOComponent/>
            <Navbar/>
            <ContactForm/>
            <Footer isDarkFooter={true}/>
        </div>
    )
}

export default ContactPage
