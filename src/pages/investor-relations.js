import React, {useEffect} from "react";
import useScroll from "../hooks/useScroll";
import Navbar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";
import Investors from "../components/Investors/Investors";
import {graphql} from "gatsby";
import SEOComponent from "../components/SEOComponent/SEOComponent";

const InvestorRelations = ({data}) => {
    const {bodyOffset, setScrollDirection} = useScroll();

    useEffect(() => {
        if(bodyOffset.top === 0) {
            setScrollDirection('down')
        }
    })

    return (
        <div className="layout_container">
            <SEOComponent/>
            <Navbar/>
            <Investors news={data.news.edges}/>
            <Footer isDarkFooter={true}/>
        </div>
    )
}

export default InvestorRelations;

export const query = graphql`
  {
    news: allContentfulNews {
        edges {
          node {
            id
            date(formatString: "DD MMM YYYY")
            slug
            description {
              description
            }
            content {
              raw
            }
            title
        }
      }
    }
  }
`