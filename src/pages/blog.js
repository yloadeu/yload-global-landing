import React from 'react'
import Navbar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";
import BlogCollection from "../components/Blog/BlogCollection/BlogCollection";
import {graphql} from "gatsby";
import SEOComponent from "../components/SEOComponent/SEOComponent";

const BlogPage = ({data}) => {
    return (
        <div className="layout_container">
            <SEOComponent/>
            <Navbar navbarClass="partner_navbar"/>
            <BlogCollection blogs={data.blogs.edges}/>
            <Footer isDarkFooter={true}/>
        </div>
    )
}

export default BlogPage

export const query = graphql`
  {
    blogs: allContentfulBlogPage {
       edges {
        node {
            slug
            title
            heroImage {
                gatsbyImage(placeholder: BLURRED, width: 3000, quality: 100)
            }
            description {
                description
            }
            tags
        }
       }
    }
  }
`