import * as React from "react"
import {graphql} from "gatsby"
import JobDescription from "../../components/Career/JobDescription/JobDescription";
import Navbar from "../../components/Navbar/Navbar";
import Footer from "../../components/Footer/Footer";
import useScroll from "../../hooks/useScroll";
import {useEffect} from "react";
import SEOComponent from "../../components/SEOComponent/SEOComponent";

function Job(props) {
    const {job} = props.data;
    const {bodyOffset, setScrollDirection} = useScroll();

    useEffect(() => {
        if (bodyOffset.top === 0) {
            setScrollDirection('down')
        }
    })

    return (
        <div className="layout_container">
            <SEOComponent/>
            <Navbar/>
            <JobDescription job={job}/>
            <Footer isDarkFooter={true}/>
        </div>
    )
}

export default Job

export const query = graphql`
  query($id: String!) {
    job(id: { eq: $id }) {
      name
      type
      description {
        firstParagraph
        secondParagraph
      }
      qualifications
      desirable
    }
  }
`
