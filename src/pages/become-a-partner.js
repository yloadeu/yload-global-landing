import * as React from 'react';
import Footer from "../components/Footer/Footer";
import Navbar from "../components/Navbar/Navbar";
import BecomePartner from "../components/BecomePartner/BecomePartner";
import useScroll from "../hooks/useScroll";
import {useEffect} from "react";
import SEOComponent from "../components/SEOComponent/SEOComponent";

const BecomeAPartnerPage = () => {
    const {bodyOffset, setScrollDirection} = useScroll();

    useEffect(() => {
        if (bodyOffset.top === 0) {
            setScrollDirection('down')
        }
    })

    return (
        <div className="layout_container">
            <SEOComponent/>
            <Navbar navbarClass="partner_navbar"/>
            <BecomePartner/>
            <Footer isDarkFooter={true}/>
        </div>
    )
}

export default BecomeAPartnerPage
