import * as React from 'react';
import useScroll from "../hooks/useScroll";
import {useEffect} from "react";
import SEOComponent from "../components/SEOComponent/SEOComponent";
import PrivacyPolicy from "../components/PrivacyPolicy/PrivacyPolicy";
import Navbar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";


const PrivacyPolicyPage = () => {
    const {bodyOffset, setScrollDirection} = useScroll();

    useEffect(() => {
        if (bodyOffset.top === 0) {
            setScrollDirection('down')
        }
    })

    return (
        <div className="layout_container">
            <SEOComponent/>
            <Navbar/>
            <PrivacyPolicy/>
            <Footer isDarkFooter={true}/>
        </div>
    )

}

export default PrivacyPolicyPage;