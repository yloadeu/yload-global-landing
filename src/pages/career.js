import * as React from 'react';
import Footer from "../components/Footer/Footer";
import Navbar from "../components/Navbar/Navbar";
import Career from "../components/Career/Career";
import useScroll from "../hooks/useScroll";
import {useEffect} from "react";
import {graphql} from "gatsby";
import SEOComponent from "../components/SEOComponent/SEOComponent";

const CareerPage = ({data}) => {
    const {bodyOffset, setScrollDirection} = useScroll();

    useEffect(() => {
        if (bodyOffset.top === 0) {
            setScrollDirection('down')
        }
    })

    return (
        <div className="layout_container">
            <SEOComponent/>
            <Navbar/>
            <Career data={data.jobs}/>
            <Footer isDarkFooter={true}/>
        </div>
    )
}

export default CareerPage

export const query = graphql`
  {
    jobs: allJob {
      nodes {
        name
        nameSlug: gatsbyPath(filePath: "/career/{job.name}")
      }
    }
  }
`
