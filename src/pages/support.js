import * as React from 'react';
import Footer from "../components/Footer/Footer";
import Navbar from "../components/Navbar/Navbar";
import useScroll from "../hooks/useScroll";
import {useEffect} from "react";
import Support from "../components/Support/Support";
import SEOComponent from "../components/SEOComponent/SEOComponent";

const SupportPage = () => {
    const {bodyOffset, setScrollDirection} = useScroll();

    useEffect(() => {
        if(bodyOffset.top === 0) {
            setScrollDirection('down')
        }
    })

    return (
        <div className="layout_container">
            <SEOComponent/>
            <Navbar/>
            <Support/>
            <Footer isDarkFooter={true}/>
        </div>
    )
}

export default SupportPage
