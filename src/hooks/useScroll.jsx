import {useState, useEffect} from "react";

const useScroll = () => {
    const [lastScrollTop, setLastScrollTop] = useState(-1);
    const [bodyOffset, setBodyOffset] = useState(typeof document !== "undefined" && document.body.getBoundingClientRect());
    const [scrollDirection, setScrollDirection] = useState("down");
    const [scrollY, setScrollY] = useState(bodyOffset.top);

    const listener = e => {
        setBodyOffset(document.body.getBoundingClientRect());
        setScrollY(-bodyOffset.top);
        setScrollDirection(lastScrollTop > -bodyOffset.top ? "down" : "up");
        setLastScrollTop(-bodyOffset.top);
    };

    useEffect(() => {
        if(scrollY < 0) {
            setScrollDirection("down");
        }
        window.addEventListener("scroll", listener);
        return () => {
            window.removeEventListener("scroll", listener);
        };
    });

    return {
        scrollY,
        setScrollY,
        bodyOffset,
        scrollDirection,
        setScrollDirection
    };
}

export default useScroll;
