import React from "react";
import * as styles from "./Blog.module.scss"
import {BLOCKS} from "@contentful/rich-text-types";
import {GatsbyImage, getImage} from "gatsby-plugin-image";
import {renderRichText} from "gatsby-source-contentful/rich-text";

const options = {
    [BLOCKS.PARAGRAPH]: (node, children) => {
        if (node.content[0].value === '') {
            return <br/>
        } else {
            return <p className={styles.content_text}>{children}</p>
        }
    },
    [BLOCKS.EMBEDDED_ASSET]: (node) => {
        const {gatsbyImage} = node.data.target
        return (
            <>
                <GatsbyImage image={getImage(gatsbyImage)} alt="" className={styles.content_image}/>
            </>
        )
    },
}

const BlogLeftSection = ({post}) => {
    return (
        <div className={styles.blog_left_section}>
            <h1 className={styles.blog_title}>{post.title}</h1>
            <div className={styles.blog_author_section}>
                <div className={styles.author_avatar}>
                    <img src={post.author.avatar.file.url} alt=""/>
                </div>
                <div className={styles.author_info}>
                    <span>{post.author.name}</span>
                    <span>{post.author.role}</span>
                </div>
                <div className={styles.publish_date}>{post.publishDate}</div>
                <div className={styles.reading_time}>{post.readingTime} min read</div>
            </div>
            <div id="article_content">{renderRichText(post.content, {renderNode: options})}</div>
        </div>
    )
}

export default BlogLeftSection;