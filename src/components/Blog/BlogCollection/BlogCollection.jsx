import React from "react";
import * as styles from "./BlogCollection.module.scss"
import {GatsbyImage, getImage} from "gatsby-plugin-image";

const BlogCollection = ({blogs}) => {
    const tags = [];
    const blogsLength = blogs.length;
    const firstFeatureBlog = blogs[blogsLength - 1];
    const secondFeatureBlog = blogs[blogsLength - 2];
    const thirdFeatureBlog = blogs[blogsLength - 3];
    blogs?.map(tag => {
        tag.node.tags?.map(item => {
            return tags.push(item);
        })
    });
    const distinctTags = [...new Set(tags)]

    return (
        <div className={styles.blog_collection_container}>
            <div className={styles.featured_post_grid}>
                <a className={styles.featured_post} href={`/${secondFeatureBlog.node.slug}`}>
                    <GatsbyImage image={getImage(secondFeatureBlog.node.heroImage)}
                                 alt='' sizes={getImage(secondFeatureBlog.node.heroImage).layout}
                                 className={styles.featured_post_cover}/>
                    {
                        secondFeatureBlog.node.tags.map(tag =>
                            <span style={{textTransform: 'capitalize'}}>{tag}</span>
                        )
                    }
                    <h3>{secondFeatureBlog.node.title}</h3>
                    <p className={styles.featured_post_description}>{secondFeatureBlog.node.description.description}</p>
                </a>
                <a className={styles.featured_post} href={`/${firstFeatureBlog.node.slug}`}>
                    <GatsbyImage image={getImage(firstFeatureBlog.node.heroImage)}
                                 alt='' sizes={getImage(firstFeatureBlog.node.heroImage).layout}
                                 className={styles.featured_post_cover}/>
                    <div>
                        {
                            firstFeatureBlog.node.tags.map(tag =>
                                <span style={{textTransform: 'capitalize'}}>{tag}</span>
                            )
                        }
                        <h3>{firstFeatureBlog.node.title}</h3>
                    </div>
                </a>
                {/*<a className={styles.featured_post}>*/}
                    {/*<GatsbyImage image={getImage(blogs[0].node.heroImage)}*/}
                    {/*             alt='' sizes={getImage(blogs[0].node.heroImage).layout}*/}
                    {/*             className={styles.featured_post_cover}/>*/}
                    {/*<div>*/}
                    {/*    {*/}
                    {/*        blogs[0].node.tags.map(tag =>*/}
                    {/*            <span>{tag}</span>*/}
                    {/*        )*/}
                    {/*    }*/}
                    {/*    <h3>{blogs[0].node.title}</h3>*/}
                    {/*</div>*/}
                {/*</a>*/}
            </div>

            <div className={`page_title_section ${styles.collection_title}`}>Explore more articles</div>
            <div className={styles.tags_section}>
                {
                    distinctTags?.map(tag =>
                        <a className={styles.tag} href="">{tag}</a>
                    )
                }
            </div>

            <div className={styles.articles_list}>
                {
                    blogs?.map(article =>
                        <>
                            <a className={styles.article} href={`/${article.node.slug}`} >
                                <GatsbyImage image={getImage(article.node.heroImage)}
                                             alt='' sizes={getImage(article.node.heroImage).layout}
                                             className={styles.article_cover}/>
                                {
                                    article.node.tags.map(tag =>
                                        <span style={{textTransform: 'capitalize'}}>{tag}</span>
                                    )
                                }
                                <h3 title={article.node.title}>{article.node.title}</h3>
                                <p className={styles.article_description}>{article.node.description.description}</p>
                            </a>
                        </>
                    )
                }
            </div>
        </div>
    )
}

export default BlogCollection;