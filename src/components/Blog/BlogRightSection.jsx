import React, {useEffect} from "react";
import * as styles from "./Blog.module.scss";
import useWindowSize from "../../hooks/useWindowSize";

const BlogRightSection = ({post}) => {
    const size = useWindowSize();

    return (
        <div className={styles.blog_right_section}>
            <div className={styles.explore_section}>
                <h2 className={styles.subsection_title}>Explore more articles</h2>
                <div className={styles.tags_container}>
                    {
                        post?.tags?.map(tag =>
                            <a style={{textTransform: 'capitalize'}} className={styles.tag} href="" key={tag}>{tag}</a>
                        )
                    }
                </div>
            </div>
            <div className={styles.more_articles_section}>
                <h2 className={styles.subsection_title}>More from Transportation</h2>
                <div className={styles.related_articles_grid}>
                    {
                        post.relatedArticles?.map(article =>
                            <a className={styles.article} href={`/${article.slug}`}>
                                <img src={article.heroImage.file.url} alt=""/>
                                <div className={styles.article_description}>
                                    <h3>{article.title}</h3>
                                    <p>{article.description.description}</p>
                                </div>
                            </a>
                        )
                    }
                </div>
            </div>
        </div>
    )
}

export default BlogRightSection;