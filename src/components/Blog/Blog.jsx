import React, {useContext, useState} from "react";
import * as styles from "./Blog.module.scss"
import * as ReactGA from "react-ga";
import {useIntl} from "gatsby-plugin-intl";
import {EmailContext} from "../../context/EmailProvider";
import BlogLeftSection from "./BlogLeftSection";
import {GatsbyImage, getImage} from "gatsby-plugin-image";
import BlogRightSection from "./BlogRightSection";
import facebook from "../../images/blog/facebook-logo.png";
import linkedin from "../../images/blog/linkedin-logo.png";
import link from "../../images/blog/link-logo.png";
import twitter from "../../images/blog/twitter-logo.png";
import useWindowSize from "../../hooks/useWindowSize";
import {Helmet} from "react-helmet";
import {Alert, Snackbar} from "@mui/material";


const Blog = ({post}) => {
    const t = useIntl();
    const {setEmail} = useContext(EmailContext);
    const heroImage = getImage(post.heroImage);
    const size = useWindowSize();
    const [alertOpen, setAlertOpen] = useState(false)

    const copyToClipboard = toCopy => {
        const el = document.createElement(`textarea`)
        el.value = toCopy
        el.setAttribute(`readonly`, ``)
        el.style.position = `absolute`
        el.style.left = `-9999px`
        document.body.appendChild(el)
        el.select()
        document.execCommand(`copy`)
        document.body.removeChild(el)
    }

    const logger = (isSubscribe) => {
        isSubscribe ? ReactGA.event({
            category: 'Subscribe',
            action: 'User clicked on "Newsletter subscribe" section'
        }) : ReactGA.event({
            category: 'Get started now',
            action: 'User clicked on "Optimise your transports" section'
        })
    };

    return (
        <>
            <Helmet title={post.title}
                    htmlAttributes={{lang: "en"}}
                    meta={[
                        {
                            property: "og:url",
                            content: 'https://yload.eu/' + post.slug
                        },
                        {
                            property: "og:type",
                            content: 'article'
                        },
                        {
                            property: "og:title",
                            content: post.title
                        },
                        {
                            property: "og:description",
                            content: post.description.description
                        },
                        {
                            property: "og:image",
                            content: `https:${post.heroImage.file.url}`
                        },
                        {
                            property: "twitter:card",
                            content: `summary_large_image`,
                        },
                        {
                            property: "twitter:creator",
                            content: post.author.name
                        },
                        {
                            property: "twitter:title",
                            content: post.title
                        },
                        {
                            property: "twitter:description",
                            content: post.description.description
                        },
                        {
                            property: "twitter:image",
                            content: `https:${post.heroImage.file.url}`
                        }
                    ]}
            />
            <div className={styles.blog_container}>
                <GatsbyImage image={heroImage} alt='' sizes={heroImage.layout} className={styles.blog_hero_container}/>
                <div className={styles.blog_content}>
                    <BlogLeftSection post={post}/>
                    {
                        size.width < 1200 && <div className={styles.share_section}>
                            <p>Share this article</p>
                            <div className={styles.symbols}>
                                <div>
                                    <a href={`https://www.facebook.com/sharer/sharer.php?u=https://yload.eu/${post.slug}`}
                                       target="_blank">
                                        <img src={facebook} alt=""/>
                                    </a>
                                    <a href={`https://www.linkedin.com/shareArticle?mini=true&url=https://yload.eu/${
                                        post.slug}&title=${post.title}&source=${post.title}`}
                                       target="_blank">
                                        <img src={linkedin} alt=""/>
                                    </a>
                                    <a href={`https://twitter.com/intent/tweet/?text=${post.title}&url=https://yload.eu${post.slug}`}
                                       target="_blank"
                                    >
                                        <img src={twitter} alt=""/>
                                    </a>
                                    <a onClick={() => {
                                        setAlertOpen(true);
                                        copyToClipboard(window.location.href)
                                    }}
                                       id="a_link" 
                                       style={{cursor: 'pointer'}}
                                    >
                                        <img src={link} alt=""/>
                                    </a>
                                </div>
                                <div>
                                    <button className={`button__get_started ${styles.button_back}`} id="blog_back"
                                            onClick={() => window.history.back()}>Back to blog
                                    </button>
                                    <button className={`button__get_started ${styles.button_next_article}`} id="blog_next">Next article
                                    </button>
                                </div>
                            </div>
                        </div>
                    }
                    <BlogRightSection post={post}/>
                </div>
                {
                    size.width >= 1200 && <div className={styles.share_section}>
                        <p>Share this article</p>
                        <div className={styles.symbols}>
                            <div>
                                <a href={`https://www.facebook.com/sharer/sharer.php?u=https://yload.eu/${post.slug}`}
                                   target="_blank">
                                    <img src={facebook} alt=""/>
                                </a>
                                <a href={`https://www.linkedin.com/shareArticle?mini=true&url=https://yload.eu/${
                                    post.slug}&title=${post.title}&source=${post.title}`}
                                   target="_blank">
                                    <img src={linkedin} alt=""/>
                                </a>
                                <a href={`https://twitter.com/intent/tweet/?text=${post.title}&url=https://yload.eu${post.slug}`}
                                   target="_blank">
                                    <img src={twitter} alt=""/>
                                </a>
                                <a onClick={() => {
                                    setAlertOpen(true);
                                    copyToClipboard(window.location.href)
                                }}
                                   id="a_link_2"
                                   style={{cursor: 'pointer'}}
                                >
                                    <img src={link} alt=""/>
                                </a>

                            </div>
                            <div className={styles.buttons_container}>
                                <button className={`button__get_started ${styles.button_back}`} id="blog_bavck_to_blog"
                                        onClick={() => window.history.back()}>Back to blog
                                </button>
                                <button className={`button__get_started ${styles.button_next_article}`} id="blog_next_2">Next article
                                </button>
                            </div>
                        </div>
                    </div>
                }
                <div className={styles.newsletter_container}>
                    <div className={styles.newsletter_text}>
                        <h2 className="page_title_section">{t.formatMessage({id: "newsletter.subscribeTitle"})}</h2>
                    </div>
                    <form className="newsletter_form">
                        <input placeholder={t.formatMessage({id: "newsletter.yourEmailPlaceholder"})} type="email"
                               onChange={(e) => setEmail(e.target.value)}/>
                        <button className="button__get_started" id="blog_subscribe"
                                onClick={() => logger(true)}>{t.formatMessage({id: "common.subscribe"})}</button>
                    </form>
                </div>
            </div>
            <Snackbar open={alertOpen} autoHideDuration={5000} onClose={() => setAlertOpen(false)}>
                <Alert onClose={() => setAlertOpen(false)} variant="filled" severity="info" sx={{width: '100%'}}>
                    Link copied!
                </Alert>
            </Snackbar>
        </>
    )
}

export default Blog;