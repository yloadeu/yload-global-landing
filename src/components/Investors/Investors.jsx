import React, {Fragment} from "react";
import * as styles from "./Investors.module.scss";
import {useIntl} from "gatsby-plugin-intl";
import linkIcon from "../../images/icon-link.svg";
import ReleaseCard from "./ReleaseCard/ReleaseCard";

const Investors = ({news}) => {
    const t = useIntl();

    console.log(news)

    const data = [
        {
            number: "10.000",
            text: t.formatMessage({id: "aboutUsPage.moreData.leads"})
        },
        {
            number: "5.000",
            text: t.formatMessage({id: "aboutUsPage.moreData.users"})
        },
        {
            number: "300",
            text: t.formatMessage({id: "aboutUsPage.moreData.calls"})
        },
        {
            number: "550K+",
            text: t.formatMessage({id: "aboutUsPage.moreData.transports"})
        }
    ]

    const releaseCardData = [
        {
            title: t.formatMessage({id: "investor.newsReleasesCard.card_1.cardTitle"}),
            text: t.formatMessage({id: "investor.newsReleasesCard.card_1.cardText"}),
            date: t.formatMessage({id: "investor.newsReleasesCard.card_1.cardDate"})
        },
        {
            title: t.formatMessage({id: "investor.newsReleasesCard.card_1.cardTitle"}),
            text: t.formatMessage({id: "investor.newsReleasesCard.card_1.cardText"}),
            date: t.formatMessage({id: "investor.newsReleasesCard.card_1.cardDate"})
        },
        {
            title: t.formatMessage({id: "investor.newsReleasesCard.card_1.cardTitle"}),
            text: t.formatMessage({id: "investor.newsReleasesCard.card_1.cardText"}),
            date: t.formatMessage({id: "investor.newsReleasesCard.card_1.cardDate"})
        },
    ]

    return (
        <div className={styles.investors_container}>
            <div className={styles.investors_header}>
                <h1 className="page_title_section">{t.formatMessage({id: "investor.pageTitle"})}</h1>
                <h3>{t.formatMessage({id: "investor.pageDescription"})}</h3>
            </div>
            <div className={styles.presentations_wrapper}>
                <div className={styles.presentations_container}>
                    <div className={styles.left_container}>
                        <h2 className="page_title_section">{t.formatMessage({id: "investor.presentations.title"})}</h2>
                        <ul>
                            <li>
                                <img src={linkIcon} alt=""/>
                                <span>{t.formatMessage({id: "investor.presentations.presentation1"})}</span>
                            </li>
                            <li>
                                <img src={linkIcon} alt=""/>
                                <span>{t.formatMessage({id: "investor.presentations.presentation2"})}</span>
                            </li>
                            <li>
                                <img src={linkIcon} alt=""/>
                                <span>{t.formatMessage({id: "investor.presentations.presentation3"})}</span>
                            </li>
                            <li>
                                <img src={linkIcon} alt=""/>
                                <span>{t.formatMessage({id: "investor.presentations.presentation4"})}</span>
                            </li>
                        </ul>
                        {/*<button*/}
                        {/*    className="button__get_started">{t.formatMessage({id: "investor.presentations.seeAllPresentations"})}</button>*/}
                    </div>
                    <div className={styles.right_container}>
                        <h2 className="page_title_section">{t.formatMessage({id: "investor.dataAbout"})}</h2>
                        <div className={styles.data_grid}>
                            {
                                data.map((d, index) =>
                                    <div key={index}>
                                        <span><strong>{d.number}</strong></span>
                                        <span>{d.text}</span>
                                    </div>
                                )
                            }
                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.releases_container}>
                <h2 className="page_title_section">{t.formatMessage({id: "investor.newsReleasesTitle"})}</h2>
                <h4>{t.formatMessage({id: "investor.newsReleasesSubtitle"})}</h4>
                <div className={styles.cards_row}>
                    {
                        news.map((card, index) =>
                            <a key={index} href={`/${card.node.slug}`} style={{textDecoration: "none", color: '#000000'}}>
                                <ReleaseCard title={card.node.title} content={card.node.description.description} date={card.node.date}/>
                            </a>
                        )
                    }
                </div>
                {/*<button className="button__get_started">{t.formatMessage({id: "investor.viewAll"})}</button>*/}
            </div>
        </div>
    )
}

export default Investors;
