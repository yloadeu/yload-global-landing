import React from "react";
import * as styles from "./ReleaseCard.module.scss";

const ReleaseCard = ({title, content, date}) => {
    return (
        <div className={styles.release_card_container}>
            <h3>{title}</h3>
            <p>{content}</p>
            <span>{date}</span>
        </div>
    )
}

export default ReleaseCard;
