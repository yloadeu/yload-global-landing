import React from 'react';
import * as styles from './News.module.scss';
import {BLOCKS} from "@contentful/rich-text-types";
import {GatsbyImage, getImage} from "gatsby-plugin-image";
import {renderRichText} from "gatsby-source-contentful/rich-text";

const options = {
    [BLOCKS.PARAGRAPH]: (node, children) => {
        if (node.content[0].value === '') {
            return <br/>
        } else {
            return <div className={styles.news}>
                <p className={styles.content_text}>{children}</p>
            </div>
        }
    },
    [BLOCKS.EMBEDDED_ASSET]: (node) => {
        const {gatsbyImage} = node.data.target
        return (
            <>
                <GatsbyImage image={getImage(gatsbyImage)} alt="" className={styles.content_image}/>
            </>
        )
    },
}

const News = ({data}) => {
    return <>
        <div className={styles.news_wrapper}>
            <div className={styles.news_container}>
                <div className={styles.news_header}>
                    <h1 className="page_title_section">{data.title}</h1>
                    <a>{data.date}</a>
                </div>
                <div className={styles.news}>
                    <p>{renderRichText(data.content, {renderNode: options})}</p>
                </div>
            </div>
        </div>
    </>
}

export default News;