import React from 'react';
import SearchRole from "./SearchRole/SearchRole";
import OpenPositions from "./OpenPositions/OpenPositions";
import PerksAndBenefits from "./PerksAndBenefits/PerksAndBenefits";
import CoreValues from "./CoreValues/CoreValues";

const Career = ({data}) => {
    return (
        <>
            <SearchRole/>
            <OpenPositions jobs={data}/>
            <PerksAndBenefits/>
            <CoreValues/>
        </>
    )
}

export default Career;
