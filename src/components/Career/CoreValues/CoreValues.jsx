import React from "react";
import * as styles from "./CoreValues.module.scss";
import {useIntl} from "gatsby-plugin-intl";
import {navigate} from 'gatsby'
import experience from "../../../images/career/experience.svg";
import transparency from "../../../images/career/transparency.svg";
import impact from "../../../images/career/impact.svg";
import grow from "../../../images/career/grow.svg";
import teamWork from "../../../images/career/team-work.svg";
import {withPrefix} from "gatsby-link";

const CoreValues = () => {
    const t = useIntl();
    const linkWithPrefix = withPrefix(`${t.locale}/career/`);

    return (
        <div className={styles.core_values_container}>
            <h2 className="page_title_section">{t.formatMessage({id: "careerPage.coreValues"})}</h2>
            <h3 className="page_subtitle_section">{t.formatMessage({id: "careerPage.mainValues"})}</h3>
            <div className={styles.values}>
                <div className={styles.value_group + " " + styles.value_group__experience}>
                    <img src={experience} alt=""/>
                    <div className={styles.value_card}>
                        <div className={styles.left_line}/>
                        <div className={styles.value_text}>
                            <h4 className={styles.value_title}>{t.formatMessage({id: "coreValuesList.customerExperience.title"})}</h4>
                            <p className={styles.value_details}>{t.formatMessage({id: "coreValuesList.customerExperience.text"})}</p>
                        </div>
                    </div>
                </div>
                <div className={styles.value_group + " " + styles.value_group__transparency}>
                    <img src={transparency} alt=""/>
                    <div className={styles.value_card}>
                        <div className={styles.left_line}/>
                        <div className={styles.value_text}>
                            <h4 className={styles.value_title}>{t.formatMessage({id: "coreValuesList.transparency.title"})}</h4>
                            <p className={styles.value_details}>{t.formatMessage({id: "coreValuesList.transparency.text"})}</p>
                        </div>
                    </div>
                </div>
                <div className={styles.value_group + " " + styles.value_group__impact}>
                    <img src={impact} alt=""/>
                    <div className={styles.value_card}>
                        <div className={styles.left_line}/>
                        <div className={styles.value_text}>
                            <h4 className={styles.value_title}>{t.formatMessage({id: "coreValuesList.impact.title"})}</h4>
                            <p className={styles.value_details}>{t.formatMessage({id: "coreValuesList.impact.text"})}</p>
                        </div>
                    </div>
                </div>
                <div className={styles.value_group + " " + styles.value_group__grow}>
                    <img src={grow} alt=""/>
                    <div className={styles.value_card}>
                        <div className={styles.left_line}/>
                        <div className={styles.value_text}>
                            <h4 className={styles.value_title}>{t.formatMessage({id: "coreValuesList.grow.title"})}</h4>
                            <p className={styles.value_details}>{t.formatMessage({id: "coreValuesList.grow.text"})}</p>
                        </div>
                    </div>
                </div>
                <div className={styles.value_group + " " + styles.value_group__team_work}>
                    <img src={teamWork} alt=""/>
                    <div className={styles.value_card}>
                        <div className={styles.left_line}/>
                        <div className={styles.value_text}>
                            <h4 className={styles.value_title}>{t.formatMessage({id: "coreValuesList.teamWork.title"})}</h4>
                            <p className={styles.value_details}>{t.formatMessage({id: "coreValuesList.teamWork.text"})}</p>
                        </div>
                    </div>
                </div>
            </div>

            <button className="button__get_started" id="core_values_open_positions"
                    onClick={() => navigate(linkWithPrefix + '#openPositions')}
            >{t.formatMessage({id: "careerPage.findOpenPositions"})}</button>
        </div>
    )
}

export default CoreValues;
