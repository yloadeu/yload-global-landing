import React from 'react';
import * as styles from "./SearchRole.module.scss";
import {useIntl} from "gatsby-plugin-intl";
import jobJson from "../../../images/animations/Job.json";
import Lottie from "react-lottie-player";

const SearchRole = () => {
    const t = useIntl();

    return (
        <div className={styles.search_vision_container}>
            <div className={styles.search_role_container}>
                <h1 className="page_title_section">{t.formatMessage({id: "careerPage.title"})}</h1>
                <h3 className="page_subtitle_section">{t.formatMessage({id: "careerPage.joinTeam"})}</h3>
                <div className="search_row">
                    <input type="search" placeholder={t.formatMessage({id: "careerPage.searchPlaceholder"})}/>
                    <button className="button__get_started" id="search_find_role">{t.formatMessage({id: "careerPage.findRole"})}</button>
                </div>
            </div>
            <div className={styles.our_vision_container}>
                <div className={styles.our_vision_text}>
                    <h2 className="page_title_section">{t.formatMessage({id: "careerPage.ourVision"})}</h2>
                    <h3 className="page_subtitle_section">{t.formatMessage({id: "careerPage.ourVisionText"})}</h3>
                </div>
                <div className={styles.our_vision_lottie}>
                    <Lottie loop
                            animationData={jobJson}
                            play
                            className={styles.lottie}
                    />
                </div>
            </div>
        </div>
    )
}

export default SearchRole;
