import React from 'react';
import * as styles from './JobDescription.module.scss';
import {Link} from 'gatsby';
import {useIntl} from "gatsby-plugin-intl";
import {withPrefix} from "gatsby-link";
import PerksAndBenefits from "../PerksAndBenefits/PerksAndBenefits";
import {splitText} from "../../../utils/utils";

const JobDescription = ({job}) => {
    const t = useIntl();
    const linkWithPrefix = withPrefix(`${t.locale}/`);

    const qualificationsList = splitText(job.qualifications);
    const desirableList = splitText(job.desirable);

    return (
        <div className={styles.job_description_wrapper}>
            <div className={styles.job_description_container}>
                <div className={styles.job_description_header}>
                    <h1 className="page_title_section">{job.name}</h1>
                    <h3 className="page_subtitle_section">{job.type}</h3>
                    <Link to={linkWithPrefix + 'career'}>View all jobs</Link>
                </div>
                <div className={styles.job_description}>
                    <p>{job.description[0].firstParagraph}</p>
                    <p>{job.description[0].secondParagraph}</p>
                    <h5>{t.formatMessage({id: "jobDescription.qualification"})} :</h5>
                    <div className={styles.description_list}>
                        {
                            qualificationsList.map((qualification, i) =>
                                <span key={i}>- {qualification}</span>
                            )
                        }
                    </div>
                    <h5>{t.formatMessage({id: "jobDescription.desirable"})} :</h5>
                    <div className={styles.description_list}>
                        {
                            desirableList.map((desirable, i) =>
                                <span key={i}>- {desirable}</span>
                            )
                        }
                    </div>
                </div>
            </div>

            <PerksAndBenefits/>

            <div className={styles.apply_with_linkedin}>
                {/*<h4>{t.formatMessage({id: "jobDescription.applyForJob"})}</h4>*/}
                {/*<p>*/}
                {/*    {t.formatMessage({id: "jobDescription.applyForJobInfo"})}*/}
                {/*    <strong>{t.formatMessage({id: "jobDescription.learnMore"})}</strong>*/}
                {/*</p>*/}
                {/*<button*/}
                {/*    className="button__get_started">{t.formatMessage({id: "jobDescription.applyWithLinkedin"})}</button>*/}
            </div>

            <div className={styles.join_team}>
                <h2 className="page_title_section">{t.formatMessage({id: "jobDescription.applyForJob"})}</h2>
                <h3 className="page_subtitle_section">Cluj-Napoca, Sibiu, Romania</h3>
                <button className="button__get_started" id="job_join_team"
                        onClick={() => window.open('https://share-eu1.hsforms.com/1K5u29snESUq8PeakmaMwmAfv7ye')}>
                    {t.formatMessage({id: "jobDescription.joinOurTeam"})}
                </button>
            </div>
        </div>
    )
}

export default JobDescription;
