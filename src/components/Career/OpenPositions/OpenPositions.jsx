import React, {useEffect, useState} from "react";
import * as styles from "./OpenPositions.module.scss";
import {useIntl} from "gatsby-plugin-intl";
import {navigate} from "gatsby";
import {withPrefix} from "gatsby-link";

const OpenPositions = ({jobs}) => {
    const t = useIntl();
    const [jobList, setJobList] = useState([]);

    useEffect(() => {
      setJobList(jobs.nodes);
      }, [jobs])

    const linkWithPrefix = withPrefix(`${t.locale}`);

    return (
        <div className={styles.open_positions_container} id="openPositions">
            <h2 className="page_title_section">{t.formatMessage({id: "careerPage.openPositions"})}</h2>
            <h3 className="page_subtitle_section">{t.formatMessage({id: "careerPage.jobOpenings"})}</h3>
            <div className={styles.filter_options_row}>
                <span>{t.formatMessage({id: "careerPage.filterBy"})}</span>
                <button className={styles.filter_button} id="open_positions_all_locations">{t.formatMessage({id: "careerPage.allLocations"})}</button>
                <button className={styles.filter_button} id="open_positions_all_teams">{t.formatMessage({id: "careerPage.allTeams"})}</button>
            </div>

            <div className={styles.positions_list_container}>
                <div className={styles.position}>
                    <button
                        className={styles.category_name} id="open_positions_title_marketing">{t.formatMessage({id: "careerPage.openPositionsList.marketing.title"})}</button>
                    <div className={styles.position_item} id="open_positions_marketing_1"
                         onClick={() => navigate(linkWithPrefix + jobList[0].nameSlug)}
                    >
                        <span>{t.formatMessage({id: "careerPage.openPositionsList.marketing.1"})}</span>
                        <span>{t.formatMessage({id: "careerPage.hybrid"})}</span>
                    </div>
                    <div className={styles.position_item} id="open_positions_marketing_2"
                         onClick={() => window.open('https://share-eu1.hsforms.com/1K5u29snESUq8PeakmaMwmAfv7ye')}
                    >
                        <span>{t.formatMessage({id: "careerPage.openPositionsList.marketing.2"})}</span>
                        <span>{t.formatMessage({id: "careerPage.hybrid"})}</span>
                    </div>
                </div>
                <div className={styles.position}>
                    <button
                        className={styles.category_name} id="open_positions_title_development">{t.formatMessage({id: "careerPage.openPositionsList.development.title"})}</button>
                    <div className={styles.position_item} id="open_positions_development_1"
                         onClick={() => window.open('https://share-eu1.hsforms.com/1K5u29snESUq8PeakmaMwmAfv7ye')}
                    >
                        <span>{t.formatMessage({id: "careerPage.openPositionsList.development.1"})}</span>
                        <span>{t.formatMessage({id: "careerPage.hybrid"})}</span>
                    </div>
                    <div className={styles.position_item} id="open_positions_development_2"
                         onClick={() => window.open('https://share-eu1.hsforms.com/1K5u29snESUq8PeakmaMwmAfv7ye')}
                    >
                        <span>{t.formatMessage({id: "careerPage.openPositionsList.development.2"})}</span>
                        <span>{t.formatMessage({id: "careerPage.hybrid"})}</span>
                    </div>
                </div>
                <div className={styles.position}>
                    <button
                        className={styles.category_name} id="open_positions_title_operations">{t.formatMessage({id: "careerPage.openPositionsList.operations.title"})}</button>
                    <div className={styles.position_item} id="open_positions_operations_1"
                         onClick={() => window.open('https://share-eu1.hsforms.com/1K5u29snESUq8PeakmaMwmAfv7ye')}
                    >
                        <span>{t.formatMessage({id: "careerPage.openPositionsList.operations.1"})}</span>
                        <span>{t.formatMessage({id: "careerPage.hybrid"})}</span>
                    </div>
                </div>
                <div className={styles.position}>
                    <button
                        className={styles.category_name} id="open_positions_title_support">{t.formatMessage({id: "careerPage.openPositionsList.support.title"})}</button>
                    <div className={styles.position_item} id="open_positions_support_1"
                         onClick={() => window.open('https://share-eu1.hsforms.com/1K5u29snESUq8PeakmaMwmAfv7ye')}
                    >
                        <span>{t.formatMessage({id: "careerPage.openPositionsList.support.1"})}</span>
                        <span>{t.formatMessage({id: "careerPage.hybridOrRemote"})}</span>
                    </div>
                </div>
            </div>
            <div
                className={`${styles.see_more} page_subtitle_section`}>{t.formatMessage({id: "careerPage.moreJobOpenings"})}</div>
        </div>
    )
}

export default OpenPositions;
