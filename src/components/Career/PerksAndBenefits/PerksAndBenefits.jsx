import React from "react";
import * as styles from "./PerksAndBenefits.module.scss";
import {useIntl} from "gatsby-plugin-intl";
import bus from "../../../images/career/Icon awesome-bus.svg";
import food from "../../../images/career/Icon map-food.svg";
import health from "../../../images/career/Icon map-health.svg";
import laptop from "../../../images/career/Icon material-laptop-mac.svg";
import school from "../../../images/career/Icon material-school.svg";
import truck from "../../../images/career/Icon awesome-truck-loading.svg";
import useWindowSize from "../../../hooks/useWindowSize";

const PerksAndBenefits = () => {
    const t = useIntl();
    const size = useWindowSize();
    return (
        <div className={styles.perks_container}>
            <h2 className="page_title_section">
                {size.width > 768 ? t.formatMessage({id: "careerPage.perksAndBenefits"})
                    : t.formatMessage({id: "careerPage.perksAndBenefitsMobile"})}</h2>
            <h3 className="page_subtitle_section">{
                size.width > 768 ? t.formatMessage({id: "careerPage.perksAndBenefitsSubtitle"})
                    : t.formatMessage({id: "careerPage.perksAndBenefitsSubtitleMobile"})}</h3>
            <div className={styles.perks_grid}>
                <div className={styles.flex_auto}>
                    <div className={styles.item + " " + styles.item__blue}>
                        <img src={laptop} alt=""/>
                    </div>
                    <span
                        className={styles.item_title}>{t.formatMessage({id: "careerPage.perksAndBenefitsList.1"})}</span>
                </div>
                <div className={styles.flex_auto}>
                    <div className={styles.item + " " + styles.item__lime}>
                        <img src={truck} alt=""/>
                    </div>
                    <span
                        className={styles.item_title}>{t.formatMessage({id: "careerPage.perksAndBenefitsList.2"})}</span>
                </div>
                <div className={styles.flex_auto}>
                    <div className={styles.item + " " + styles.item__orange}>
                        <img src={bus} alt=""/>
                    </div>
                    <span
                        className={styles.item_title}>{t.formatMessage({id: "careerPage.perksAndBenefitsList.3"})}</span>
                </div>
                <div className={styles.flex_auto}>
                    <div className={styles.item + " " + styles.item__green}>
                        <img src={food} alt=""/>
                    </div>
                    <span
                        className={styles.item_title}>{t.formatMessage({id: "careerPage.perksAndBenefitsList.4"})}</span>
                </div>
                <div className={styles.flex_auto}>
                    <div className={styles.item + " " + styles.item__red}>
                        <img src={health} alt=""/>
                    </div>
                    <span
                        className={styles.item_title}>{t.formatMessage({id: "careerPage.perksAndBenefitsList.5"})}</span>
                </div>
                <div className={styles.flex_auto}>
                    <div className={styles.item + " " + styles.item__purple}>
                        <img src={school} alt=""/>
                    </div>
                    <span
                        className={styles.item_title}>{t.formatMessage({id: "careerPage.perksAndBenefitsList.6"})}</span>
                </div>
            </div>
        </div>
    )
}

export default PerksAndBenefits;
