import React, {useState} from "react";
import * as styles from "./ContactForm.module.scss"
import {useIntl} from "gatsby-plugin-intl";
import {useForm} from "react-hook-form";
import ContactAdvantages from "./ContactAdvantages";
import {Widget} from "@typeform/embed-react";
import * as ReactGA from "react-ga";

const ContactForm = () => {
    const t = useIntl();
    const {register, handleSubmit, formState: {errors}} = useForm();
    const onSubmit = data => console.log(data);

    function getFlagEmoji(countryCode) {
        const codePoints = countryCode
            .toUpperCase()
            .split('')
            .map(char => 127397 + char.charCodeAt());
        return String.fromCodePoint(...codePoints);
    }

    const logger = () => {
        ReactGA.event({
            category: 'Let’s go',
            action: 'User clicked on "Contact" form'
        })
    };

    return (
        <div className={styles.contact_form_container}>
            <ContactAdvantages className={styles.contact_advantages_container}/>
            <div className={styles.contact_buttons_section}>
                <button style={{marginRight: '30px'}} className={`${styles.button} + ${styles.button_network}`}
                        onClick={() => {
                            logger();
                            t.locale === 'ro' ? window.open('https://share-eu1.hsforms.com/1WOggoIkpSEKbN5j9IAufZQfv7ye') :
                                window.open('https://share-eu1.hsforms.com/1jEhrhVCQQES1B_bPATuZ8Afv7ye')
                        }}>YNetwork
                </button>
                <button className={`${styles.button} + ${styles.button_trucks}`} onClick={() => {
                    logger();
                    t.locale === 'ro' ? window.open('https://share-eu1.hsforms.com/1QBYJOzIWRNCOOQomhqKBGAfv7ye') :
                        window.open('https://share-eu1.hsforms.com/1s_SB9dP2Qjm6ZHHiTE7U3Qfv7ye')
                }}>YTrucks
                </button>
            </div>

            { /* Contact Form  */}
            {/*<form className={styles.form} onSubmit={handleSubmit(onSubmit)}>*/}
            {/*    <div className={styles.form_group}>*/}
            {/*        <label>{t.formatMessage({id: "contactPage.firstName"})} <span>*</span></label>*/}
            {/*        <input className={styles.form_input} {...register("firstName", {*/}
            {/*            required: {*/}
            {/*                value: true,*/}
            {/*                message: t.formatMessage({id: "common.requiredField"})*/}
            {/*            }*/}
            {/*        })} />*/}
            {/*        {errors.firstName && <span className={styles.error}>{errors.firstName.message}</span>}*/}
            {/*    </div>*/}

            {/*    <div className={styles.form_group}>*/}
            {/*        <label>{t.formatMessage({id: "contactPage.lastName"})} <span>*</span></label>*/}
            {/*        <input className={styles.form_input} {...register("lastName", {*/}
            {/*            required: {*/}
            {/*                value: true,*/}
            {/*                message: t.formatMessage({id: "common.requiredField"})*/}
            {/*            }*/}
            {/*        })} />*/}
            {/*        {errors.lastName && <span className={styles.error}>{errors.lastName.message}</span>}*/}
            {/*    </div>*/}
            {/*    <div className={styles.form_group}>*/}
            {/*        <label>{t.formatMessage({id: "contactPage.workEmail"})} <span>*</span></label>*/}
            {/*        <input className={styles.form_input} type="email" {...register("email", {*/}
            {/*            required: {*/}
            {/*                value: true,*/}
            {/*                message: t.formatMessage({id: "common.requiredField"})*/}
            {/*            }*/}
            {/*        })} />*/}
            {/*        {errors.email && <span className={styles.error}>{errors.email.message}</span>}*/}
            {/*    </div>*/}
            {/*    <div className={styles.form_group}>*/}
            {/*        <label>{t.formatMessage({id: "contactPage.jobTitle"})} <span>*</span></label>*/}
            {/*        <input className={styles.form_input} {...register("jobTitle", {*/}
            {/*            required: {*/}
            {/*                value: true,*/}
            {/*                message: t.formatMessage({id: "common.requiredField"})*/}
            {/*            }*/}
            {/*        })} />*/}
            {/*        {errors.jobTitle && <span className={styles.error}>{errors.jobTitle.message}</span>}*/}
            {/*    </div>*/}
            {/*    <div className={styles.form_group}>*/}
            {/*        <label>{t.formatMessage({id: "contactPage.phoneNumber"})} <span>*</span></label>*/}
            {/*        <div className={styles.form_input + " " + styles.form_input_wrapper}>*/}
            {/*            <select className={styles.inner_input_flags}>*/}
            {/*                <option>{getFlagEmoji("RO")}</option>*/}
            {/*                <option>{getFlagEmoji("GB")}</option>*/}
            {/*            </select>*/}
            {/*            <input className={styles.inner_input_phone} {...register("phoneNumber", {*/}
            {/*                required: {*/}
            {/*                    value: true,*/}
            {/*                    message: t.formatMessage({id: "common.requiredField"})*/}
            {/*                },*/}
            {/*                pattern: {*/}
            {/*                    value: /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$/g,*/}
            {/*                    message: t.formatMessage({id: "common.invalidPhone"})*/}
            {/*                }*/}
            {/*            })}/>*/}
            {/*        </div>*/}
            {/*        {errors.phoneNumber && <span className={styles.error}>{errors.phoneNumber.message}</span>}*/}
            {/*    </div>*/}
            {/*    <div className={styles.form_group}>*/}
            {/*        <label>{t.formatMessage({id: "contactPage.companyName"})} <span>*</span></label>*/}
            {/*        <input className={styles.form_input} {...register("companyName", {*/}
            {/*            required: {*/}
            {/*                value: true,*/}
            {/*                message: t.formatMessage({id: "common.requiredField"})*/}
            {/*            }*/}
            {/*        })} />*/}
            {/*        {errors.companyName && <span className={styles.error}>{errors.companyName.message}</span>}*/}
            {/*    </div>*/}
            {/*    <div className={styles.form_group}>*/}
            {/*        <label>{t.formatMessage({id: "contactPage.companySize"})} <span>*</span></label>*/}
            {/*        <input className={styles.form_input}*/}
            {/*               placeholder={t.formatMessage({id: "contactPage.selectPlaceholder"})} {...register("companySize", {*/}
            {/*            required: {*/}
            {/*                value: true,*/}
            {/*                message: t.formatMessage({id: "common.requiredField"})*/}
            {/*            }*/}
            {/*        })} />*/}
            {/*        {errors.companySize && <span className={styles.error}>{errors.companySize.message}</span>}*/}
            {/*    </div>*/}
            {/*    <div className={styles.form_group}>*/}
            {/*        <label>{t.formatMessage({id: "contactPage.helpMessage"})}</label>*/}
            {/*        <textarea rows="4" className={styles.form_input}*/}
            {/*                  placeholder={t.formatMessage({id: "contactPage.messagePlaceholder"})} {...register("message")} />*/}
            {/*    </div>*/}
            {/*    <span className={styles.terms}>{t.formatMessage({id: "contactPage.acceptTerms"})}</span>*/}
            {/*    <button type="submit"*/}
            {/*            className="button__get_started">{t.formatMessage({id: "contactPage.submit"})}</button>*/}
            {/*</form>*/}
        </div>
    )
}

export default ContactForm
