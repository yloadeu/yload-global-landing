import React from 'react';
import * as styles from './ContactForm.module.scss';
// import * as styles from './Contact.module.scss';
import iconMail from "../../images/home/save-time/icon-feather-mail.svg";
import iconFileText from "../../images/home/save-time/icon-feather-file-text.svg";
import iconPhone from "../../images/home/save-time/icon-feather-phone.svg";
import iconLocation from "../../images/home/save-time/location.svg";
import {useIntl} from "gatsby-plugin-intl";

const ContactAdvantages = () => {
    const t = useIntl();
    return (
        <div className={`${styles.contact_advantages_container} advantages_text`}>
            <h1 className="page_title_section">{t.formatMessage({id: "contactPage.title"})}</h1>
            {/*<div className={styles.advantages_grid}>*/}
                <div className="advantage">
                    <div className="advantage_icon_background">
                        <div><img src={iconMail} alt="icon-mail"/></div>
                    </div>
                    <div className="advantage_info">
                        <h4>{t.formatMessage({id: "saveTime.declutterOffice.title"})}</h4>
                        <p>{t.formatMessage({id: "saveTime.declutterOffice.description"})}</p>
                    </div>
                </div>
                <div className="advantage">
                    <div className="advantage_icon_background">
                        <div><img src={iconFileText} alt="icon-file-text"/></div>
                    </div>
                    <div className="advantage_info">
                        <h4>{t.formatMessage({id: "saveTime.savePaper.title"})}</h4>
                        <p>{t.formatMessage({id: "saveTime.savePaper.description"})}</p>
                    </div>
                </div>
                <div className="advantage">
                    <div className="advantage_icon_background">
                        <div><img src={iconPhone} alt="icon-phone"/></div>
                    </div>
                    <div className="advantage_info">
                        <h4>{t.formatMessage({id: "saveTime.endlessCalls.title"})}</h4>
                        <p>{t.formatMessage({id: "saveTime.endlessCalls.description"})}</p>
                        <p><a href="tel:+40377101515">+40 377 101 515</a> - {t.formatMessage({id: "contactPage.callUs"})}</p>
                    </div>
                </div>
                {/*<div className="advantage">*/}
                {/*    <div className="advantage_icon_background">*/}
                {/*        <div><img src={iconLocation} alt="icon-location"/></div>*/}
                {/*    </div>*/}
                {/*    <div className="advantage_info">*/}
                {/*        <h4>{t.formatMessage({id: "contactPage.onePlaceTitle"})}</h4>*/}
                {/*        <p>{t.formatMessage({id: "contactPage.onePlaceDescription"})}</p>*/}
                {/*    </div>*/}
                {/*</div>*/}
            {/*</div>*/}
        </div>
    )
}

export default ContactAdvantages
