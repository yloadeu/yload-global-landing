import React from "react";
import * as styles from "./ContactForm.module.scss"
import {useIntl} from "gatsby-plugin-intl";
import ContactAdvantages from "./ContactAdvantages";

const Contact = () => {
    const t = useIntl();


    return (
        <div className={styles.contact_container}>
            <ContactAdvantages className={styles.contact_advantages_container}/>
            <button type="submit"
                    className="button__get_started"
                    id="contact_submit"
                    onClick={() => window.open('https://form.typeform.com/to/JhzxOQF2')}
            >{t.formatMessage({id: "common.contactUs"})}</button>
        </div>
    )
}

export default Contact
