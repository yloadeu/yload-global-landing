import React, {useEffect, useState} from "react";
import * as styles from './AboutUsHeader.module.scss';
import YouTube from "react-youtube";
import {useIntl} from "gatsby-plugin-intl";
import {navigate} from "gatsby";
import useWindowSize from "../../../hooks/useWindowSize";
import playButton from "../../../images/about/play-button.svg";
import youtube from "../../../images/about/youtube-channel.png";

const AboutUsHeader = () => {
    const t = useIntl();
    const size = useWindowSize();
    const [videoHeight, setVideoHeight] = useState(720);
    const [videoStyle, setVideoStyle] = useState();

    const videoId = t.locale === 'ro' ? 'kxOmZnojURg' : '1K-B2u0eK14';

    useEffect(() => {
        if (size.width > 1200) {
            setVideoHeight(size.width / 3);
            setVideoStyle({height: size.width / 3});
        }
        if (size.width > 768) {
            setVideoHeight(size.width / 2);
        } else {
            setVideoHeight(size.width / 2 + 20);
            setVideoStyle({height: "100%"});
        }

    }, [size])

    const handleOnReady = (event) => {
        event.target.pauseVideo();
    }


    return (
        <>
            <div className={styles.about_header_container}>
                <div className={styles.about_header_content}>
                    <div className={styles.about_header_text_section}>
                        <h1 className="page_title_section">{t.formatMessage({id: "aboutUsPage.title"})}</h1>
                        <h3 className="page_subtitle_section">{t.formatMessage({id: "aboutUsPage.subtitle"})}</h3>
                        {
                            size.width > 1200 && <>
                                <div className={styles.play_video_row}>
                                    <img src={playButton} alt=""/>
                                    <span>{t.formatMessage({id: "aboutUsPage.watchVideo"})}</span>
                                </div>
                                <div className={styles.play_video_row}
                                     id="about_us_subscribe"
                                     style={{cursor: "pointer"}}
                                     onClick={() => window.open("https://www.youtube.com/channel/UCV5aJQgZ8NZNC0ypmfASCtg")}>
                                    <img src={youtube} alt=""/>
                                    <span>{t.formatMessage({id: "aboutUsPage.subscribe"})}</span>
                                </div>
                            </>
                        }

                    </div>
                    <div className={styles.about_header_video} >
                        <YouTube
                            containerClassName={styles.youtube_video}
                            videoId={videoId}
                            opts={{
                                height: videoHeight < 720 ? videoHeight : size.width > 2000 ? 720 : 600,
                                width: '1000'
                            }}
                            onReady={(e) => handleOnReady(e)}/>
                        {
                            size.width <= 1200 && <div className={styles.icon_section_row}>
                                <div className={styles.play_video_row}>
                                    <img src={playButton} alt=""/>
                                    <span>{t.formatMessage({id: "aboutUsPage.watchVideo"})}</span>
                                </div>
                                <div className={styles.play_video_row}
                                     id="about_us_subscribe_2"
                                     style={{cursor: "pointer"}}
                                     onClick={() => window.open("https://www.youtube.com/channel/UCV5aJQgZ8NZNC0ypmfASCtg")}>
                                    <img src={youtube} alt=""/>
                                    <span>{t.formatMessage({id: "aboutUsPage.subscribe"})}</span>
                                </div>
                            </div>
                        }
                    </div>
                </div>
            </div>
        </>
    )
}

export default AboutUsHeader;
