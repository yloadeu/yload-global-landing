import React from "react";
import * as styles from "../AboutUsContent.module.scss";
import {useIntl} from "gatsby-plugin-intl";
import {graphql, useStaticQuery} from "gatsby"
import Image from "gatsby-image";

export const getImages = graphql`
  {
    image1: file(relativePath: { eq: "about/people-img-1.png" }) {
      childImageSharp { 
        fluid(quality: 100, webpQuality: 100, maxWidth: 1080) {
          ...GatsbyImageSharpFluid_noBase64
        }
      }
    }
    
    image2: file(relativePath: { eq: "about/people-img-2.png" }) {
      childImageSharp { 
        fluid(quality: 100, webpQuality: 100, maxWidth: 1080) {
          ...GatsbyImageSharpFluid_noBase64
        }
      }
    }
    
    image3: file(relativePath: { eq: "about/people-img-3.png" }) {
      childImageSharp { 
        fluid(quality: 100, webpQuality: 100, maxWidth: 1080) {
          ...GatsbyImageSharpFluid_noBase64
        }
      }
    }
    
    image4: file(relativePath: { eq: "about/people-img-4.png" }) {
      childImageSharp { 
        fluid(quality: 100, webpQuality: 100, maxWidth: 1080) {
          ...GatsbyImageSharpFluid_noBase64
        }
      }
    }
    
    image5: file(relativePath: { eq: "about/people-img-5.png" }) {
      childImageSharp { 
        fluid(quality: 100, webpQuality: 100, maxWidth: 1080) {
          ...GatsbyImageSharpFluid_noBase64
        }
      }
    }
  }
`

const AboutPeople = () => {
    const t = useIntl();

    const {
        image1, image2, image3, image4, image5
    } = useStaticQuery(getImages)

    const values = [
        {
            id: "experience",
            title: t.formatMessage({id: "coreValuesList.customerExperience.title"}),
            text: t.formatMessage({id: "coreValuesList.customerExperience.text"})
        },
        {
            id: "transparency",
            title: t.formatMessage({id: "coreValuesList.transparency.title"}),
            text: t.formatMessage({id: "coreValuesList.transparency.text"})
        },
        {
            id: "grow",
            title: t.formatMessage({id: "coreValuesList.grow.title"}),
            text: t.formatMessage({id: "coreValuesList.grow.text"})
        },
        {
            id: "team_work",
            title: t.formatMessage({id: "coreValuesList.teamWork.title"}),
            text: t.formatMessage({id: "coreValuesList.teamWork.text"})
        },
        {
            id: "impact",
            title: t.formatMessage({id: "coreValuesList.impact.title"}),
            text: t.formatMessage({id: "coreValuesList.impact.text"})
        },
    ]

    return (
        <div className={styles.about_people_container}>
            <h2 className="page_title_section">{t.formatMessage({id: "aboutUsPage.aboutPeople"})}</h2>
            <h3 className="page_subtitle_section">{t.formatMessage({id: "aboutUsPage.ourCulture"})}</h3>
            <div className={styles.about_people_content}>
                {/*<div className={styles.images_container}>*/}
                    {/*<Image fluid={image1.childImageSharp.fluid} alt=""/>*/}
                    {/*<Image fluid={image2.childImageSharp.fluid} alt=""/>*/}
                    {/*<Image fluid={image3.childImageSharp.fluid} alt=""/>*/}
                    {/*<Image fluid={image4.childImageSharp.fluid} alt=""/>*/}
                    {/*<Image fluid={image5.childImageSharp.fluid} alt=""/>*/}
                {/*</div>*/}
                <div className={styles.values_container}>
                    {
                        values.map((value) =>
                            <div className={`${styles.value_card} ${styles[value.id]}`} key={value.id}>
                                <div className={styles.left_line}/>
                                <div className={styles.value_text}>
                                    <h4 className={styles.value_title}>{value.title}</h4>
                                    <p className={styles.value_details}>{value.text}</p>
                                </div>
                            </div>
                        )}
                </div>
            </div>
        </div>
    )
}

export default AboutPeople;
