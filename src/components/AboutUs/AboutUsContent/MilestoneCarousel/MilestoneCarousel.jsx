import React from "react";
import * as styles from "./MilestoneCarousel.module.scss";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import {useIntl} from "gatsby-plugin-intl";

const MilestoneCarousel = () => {
    const t = useIntl();

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToScroll: 1,
        slidesToShow: 1,
    }
    return (
        <Slider {...settings}>
            <div className={styles.milestone_card_container}>
                <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                    <h3>{t.formatMessage({id: "aboutUsPage.ourMilestonesCarousel.1.title"})}</h3>
                    <h4>{t.formatMessage({id: "aboutUsPage.ourMilestonesCarousel.1.subtitle"})}</h4>
                    {/*<p>{ t.formatMessage({id: "aboutUsPage.ourMilestonesCarousel.1.description"}) }</p>*/}
                </div>
            </div>
            <div className={styles.milestone_card_container}>
                <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                    <h3>{t.formatMessage({id: "aboutUsPage.ourMilestonesCarousel.2.title"})}</h3>
                    <h4>{t.formatMessage({id: "aboutUsPage.ourMilestonesCarousel.2.subtitle"})}</h4>
                    {/*<p>{ t.formatMessage({id: "aboutUsPage.ourMilestonesCarousel.2.description"}) }</p>*/}
                </div>
            </div>
            <div className={styles.milestone_card_container}>
                <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                    <h3>{t.formatMessage({id: "aboutUsPage.ourMilestonesCarousel.3.title"})}</h3>
                    <h4>{t.formatMessage({id: "aboutUsPage.ourMilestonesCarousel.3.subtitle"})}</h4>
                    {/*<p>{ t.formatMessage({id: "aboutUsPage.ourMilestonesCarousel.3.description"}) }</p>*/}
                </div>
            </div>
            <div className={styles.milestone_card_container}>
                <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                    <h3>{t.formatMessage({id: "aboutUsPage.ourMilestonesCarousel.4.title"})}</h3>
                    <h4>{t.formatMessage({id: "aboutUsPage.ourMilestonesCarousel.4.subtitle"})}</h4>
                    {/*<p>{ t.formatMessage({id: "aboutUsPage.ourMilestonesCarousel.4.description"}) }</p>*/}
                </div>

            </div>
        </Slider>
    )
}

export default MilestoneCarousel;
