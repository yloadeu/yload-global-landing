import React from "react";
import * as styles from "./AboutUsContent.module.scss";
import ourVisionImage from "../../../images/about/our-vision.webp";
import {useIntl} from "gatsby-plugin-intl";
import MilestoneCarousel from "./MilestoneCarousel/MilestoneCarousel";
import AboutPeople from "./AboutPeople/AboutPeople";
import SocialSection from "./SocialSection/SocialSection";
import * as ReactGA from "react-ga";

const AboutUsContent = () => {
    const t = useIntl();

    const data = [
        {
            number: "10.000",
            text: t.formatMessage({id: "aboutUsPage.moreData.leads"})
        },
        {
            number: "5.000",
            text: t.formatMessage({id: "aboutUsPage.moreData.users"})
        },
        {
            number: "300",
            text: t.formatMessage({id: "aboutUsPage.moreData.calls"})
        },
        {
            number: "550K+",
            text: t.formatMessage({id: "aboutUsPage.moreData.transports"})
        }
    ]

    const logger = () => {
        ReactGA.event({
            category: 'Get started now',
            action: 'User clicked on "About us" page'
        })
    };

    return (
        <div className={styles.about_content_container}>
            <div className={styles.content_our_vision}>
                <div className={styles.our_vision_col}>
                    <h2 className="page_title_section">{t.formatMessage({id: "aboutUsPage.ourVision"})}</h2>
                    <h3 className="page_subtitle_section">{t.formatMessage({id: "aboutUsPage.ourVisionText"})}</h3>
                </div>
                <div className={styles.our_vision_col}>
                    <img src={ourVisionImage} alt=""/>
                </div>
            </div>

            <div className={styles.content_our_milestone}>
                <h2 className="page_title_section">{t.formatMessage({id: "aboutUsPage.ourMilestones"})}</h2>
                <MilestoneCarousel/>
            </div>

            <div className={styles.content_more_data}>
                <h2 className="page_title_section"> {t.formatMessage({id: "aboutUsPage.moreData.title"})} </h2>
                <div className={styles.data_grid}>
                    {
                        data.map((d, index) =>
                            <div key={index}>
                                <span><strong>{d.number}</strong></span>
                                <span>{d.text}</span>
                            </div>
                        )
                    }
                </div>
            </div>

            <AboutPeople />
            <SocialSection/>

            <div className={styles.more_together}>
                <h2 className="page_title_section">{ t.formatMessage({id: "aboutUsPage.moreTogether"}) }</h2>
                <div className={styles.button_section}>
                    <button className="button__get_started" id="about_us_get_started"
                            onClick={() => {
                                logger();
                                t.locale === 'ro' ? window.open('https://share-eu1.hsforms.com/1QBYJOzIWRNCOOQomhqKBGAfv7ye') :
                                    window.open('https://share-eu1.hsforms.com/1s_SB9dP2Qjm6ZHHiTE7U3Qfv7ye')
                            }}>
                        {t.formatMessage({id: "common.getStartedNow"})}
                    </button>
                    <h6>{t.formatMessage({id: "header.freePeriod"})}
                        <br/>{t.formatMessage({id: "header.creditCard"})}
                    </h6>
                </div>
            </div>

        </div>
    )
}

export default AboutUsContent;
