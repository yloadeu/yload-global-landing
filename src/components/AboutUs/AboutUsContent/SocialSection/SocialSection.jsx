import React from "react";
import * as styles from "../AboutUsContent.module.scss";
import {useIntl} from "gatsby-plugin-intl";
import facebookLogo from "../../../../images/footer/facebook-logo.svg";
import instagramLogo from "../../../../images/footer/instagram-logo.svg";
import youtubeLogo from "../../../../images/footer/youtube-logo.svg";
import linkedinLogo from "../../../../images/footer/linkedin-logo.svg";

const SocialSection = () => {
    const t = useIntl();

    return (
        <div className={styles.social_container}>
            <h2 className="page_title_section">{t.formatMessage({id: "aboutUsPage.socialTitle"})}</h2>
            <div className={styles.social_icons_row}>
                <a href="https://m.facebook.com/yloadeu/" target="_blank" rel="noreferrer">
                    <img src={facebookLogo} alt=""/>
                </a>
                <a href="https://www.instagram.com/yload_/" target="_blank" rel="noreferrer">
                    <img src={instagramLogo} alt=""/>
                </a>
                <a href="https://www.youtube.com/channel/UCV5aJQgZ8NZNC0ypmfASCtg" target="_blank"
                   rel="noreferrer">
                    <img src={youtubeLogo} alt=""/>
                </a>
                <a href="https://www.linkedin.com/company/yload/" target="_blank">
                    <img src={linkedinLogo} alt=""/>
                </a>
            </div>
        </div>
    )
}

export default SocialSection;
