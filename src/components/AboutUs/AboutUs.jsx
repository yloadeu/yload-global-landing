import React from "react";
import AboutUsHeader from "./AboutUsHeader/AboutUsHeader";
import AboutUsContent from "./AboutUsContent/AboutUsContent";

const AboutUs = () => {
    return (
        <>
            <AboutUsHeader/>
            <AboutUsContent/>
        </>
    )
}

export default AboutUs;
