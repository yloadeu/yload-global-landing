import React from 'react';
import * as styles from './PrivacyPolicy.module.scss';
import {useIntl} from "gatsby-plugin-intl";
import {withPrefix} from "gatsby-link";

const PrivacyPolicy = () => {
    const t = useIntl();
    const linkWithPrefix = withPrefix(`${t.locale}/`);

    return (
        <div className={styles.privacy_wrapper}>
            <div className={styles.privacy_container}>
                <div className={styles.privacy_header}>
                    <h1 className="page_title_section">{t.formatMessage({id: "privacyPolicy.title"})}</h1>
                </div>
                <div className={styles.privacy_text}>
                    <h5>{t.formatMessage({id: "privacyPolicy.intro.title"})}</h5>
                    <p>{t.formatMessage({id: "privacyPolicy.intro.description"})}</p>
                    <h5>{t.formatMessage({id: "privacyPolicy.personalDataText.title"})}</h5>
                    <p>{t.formatMessage({id: "privacyPolicy.personalDataText.description"})}</p>
                    <div className={styles.description_list}>
                        <span>{t.formatMessage({id: "privacyPolicy.personalDataText.1"})}</span>
                        <span>{t.formatMessage({id: "privacyPolicy.personalDataText.2"})}</span>
                        <span>{t.formatMessage({id: "privacyPolicy.personalDataText.3"})}</span>
                    </div>

                    <h5>{t.formatMessage({id: "privacyPolicy.usePersonalData.title"})}</h5>
                    <p>{t.formatMessage({id: "privacyPolicy.usePersonalData.description"})}</p>
                    <div className={styles.description_list}>
                        <span>{t.formatMessage({id: "privacyPolicy.usePersonalData.1"})}</span>
                        <span>{t.formatMessage({id: "privacyPolicy.usePersonalData.2"})}</span>
                        <span>{t.formatMessage({id: "privacyPolicy.usePersonalData.3"})}</span>
                        <span>{t.formatMessage({id: "privacyPolicy.usePersonalData.4"})}</span>
                        <span>{t.formatMessage({id: "privacyPolicy.usePersonalData.5"})}</span>
                        <span>{t.formatMessage({id: "privacyPolicy.usePersonalData.6"})}</span>
                        {
                            t.locale !== 'ro' && <span>{t.formatMessage({id: "privacyPolicy.usePersonalData.7"})}</span>
                        }
                    </div>

                    <h5>{t.formatMessage({id: "privacyPolicy.securityPersonalData.title"})}</h5>
                    <p>{t.formatMessage({id: "privacyPolicy.securityPersonalData.description"})}</p>

                    <h5>{t.formatMessage({id: "privacyPolicy.rights.title"})}</h5>
                    <p>{t.formatMessage({id: "privacyPolicy.rights.description"})}</p>
                    <div className={styles.description_list}>
                        <span>{t.formatMessage({id: "privacyPolicy.rights.1"})}</span>
                        <span>{t.formatMessage({id: "privacyPolicy.rights.2"})}</span>
                        <span>{t.formatMessage({id: "privacyPolicy.rights.3"})}</span>
                        <span>{t.formatMessage({id: "privacyPolicy.rights.4"})}</span>
                        <span>{t.formatMessage({id: "privacyPolicy.rights.5"})}</span>
                        {
                            t.locale === 'ro' && <>
                                <span>{t.formatMessage({id: "privacyPolicy.rights.6"})}</span>
                                <span>{t.formatMessage({id: "privacyPolicy.rights.7"})}</span>
                                <span>{t.formatMessage({id: "privacyPolicy.rights.8"})}</span>
                                <span>{t.formatMessage({id: "privacyPolicy.rights.9"})}</span>
                            </>
                        }
                    </div>

                    <h5>{t.formatMessage({id: "privacyPolicy.updates"})}</h5>
                    <p>{t.formatMessage({id: "privacyPolicy.updatesDescription"})}</p>

                    <h5>{t.formatMessage({id: "privacyPolicy.contact"})}</h5>
                    <p>{t.formatMessage({id: "privacyPolicy.contactDescription"})}</p>

                    <div className={styles.contact_details}>
                        <p>Yload Global SA, </p>
                        <p>Ana Aslan, nr. 40, Cluj-Napoca, Romania </p>
                        <p>CUI: RO41337497 | J12/2703/2019</p>
                        <p>business@yload.ro</p>
                        <p>+40 377 101 515</p>
                    </div>

                    <p>{t.formatMessage({id: "privacyPolicy.revisionDate"})}
                        <span style={{marginLeft: '10px', fontStyle: 'italic'}}>24/03/2013</span></p>
                </div>
            </div>
        </div>
    )

}

export default PrivacyPolicy;