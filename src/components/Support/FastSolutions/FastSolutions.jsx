import React from "react";
import * as styles from "./FastSolutions.module.scss"
import {useIntl} from "gatsby-plugin-intl";

const FastSolutions = () => {
    const t = useIntl();
    return(
        <div className={styles.fast_solution_container}>
            <h2 className="page_title_section">{t.formatMessage({id: "supportPage.fastSolutions.title"})}</h2>
            <div className={styles.cards_row}>
                <div className={styles.card}>
                    <p>{t.formatMessage({id: "supportPage.fastSolutions.1"})}</p>
                </div>
                <div className={styles.card}>
                    <p>{t.formatMessage({id: "supportPage.fastSolutions.2"})}</p>
                </div>
                <div className={styles.card}>
                    <p>{t.formatMessage({id: "supportPage.fastSolutions.3"})}</p>
                </div>
                <div className={styles.card}>
                    <p>{t.formatMessage({id: "supportPage.fastSolutions.4"})}</p>
                </div>
            </div>
        </div>
    )
}

export default FastSolutions;
