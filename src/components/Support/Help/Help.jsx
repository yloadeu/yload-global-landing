import React from "react";
import * as styles from "./Help.module.scss";
import {useIntl} from "gatsby-plugin-intl";
import ylYellow from '../../../images/support/ylYellow.svg';
import ylBlue from '../../../images/support/ylBlue.svg';
import ylRed from '../../../images/support/ylRed.svg';
import ylLime from '../../../images/support/ylLime.svg';
import Lottie from "react-lottie-player";
import arrowJson from "../../../images/animations/arrow.json";
import useWindowSize from "../../../hooks/useWindowSize";

const Help = () => {
    const t = useIntl();
    const size = useWindowSize();
    return (
        <div className={styles.help_container}>
            <div className={styles.search_container}>
                <h1 className="page_title_section">{t.formatMessage({id: "supportPage.title"})}</h1>
                <h3 className="page_subtitle_section">{t.formatMessage({id: "supportPage.searchText"})}</h3>
                <div className="search_row">
                    <input type="search" placeholder={t.formatMessage({id: "supportPage.searchPlaceholder"})}/>
                    <button className="button__get_started" id="help_find_out">{t.formatMessage({id: "supportPage.findOut"})}</button>
                </div>
            </div>
            <div className={styles.cards_row}>
                <div className={styles.card_wrapper}>

                    <div className={styles.card_container}>
                        <img src={ylYellow} alt="" className={styles.image__ylYellow}/>
                        <img src={ylRed} alt="" className={styles.image__ylRed}/>
                        <span
                            className={styles.card_title}>{t.formatMessage({id: "supportPage.supportCards.knowledgeBase.title"})}</span>
                        <ul className={styles.links}>
                            <li>{t.formatMessage({id: "supportPage.supportCards.knowledgeBase.1"})}</li>
                            <li>{t.formatMessage({id: "supportPage.supportCards.knowledgeBase.2"})}</li>
                            <li>{t.formatMessage({id: "supportPage.supportCards.knowledgeBase.3"})}</li>
                            <li>{t.formatMessage({id: "supportPage.supportCards.knowledgeBase.4"})}</li>
                            <li>{t.formatMessage({id: "supportPage.supportCards.knowledgeBase.5"})}</li>
                            <li>{t.formatMessage({id: "supportPage.supportCards.knowledgeBase.6"})}</li>
                        </ul>
                    </div>
                </div>
                <div className={styles.card_wrapper}>
                    <div className={styles.card_container}>
                        <img src={ylLime} alt="" className={styles.image__ylLime}/>
                        <span
                            className={styles.card_title}>{t.formatMessage({id: "supportPage.supportCards.solutions.title"})}</span>
                        <ul>
                            <li>{t.formatMessage({id: "supportPage.supportCards.solutions.1"})}</li>
                            <li>{t.formatMessage({id: "supportPage.supportCards.solutions.2"})}</li>
                            <li>{t.formatMessage({id: "supportPage.supportCards.solutions.3"})}</li>
                            <li>{t.formatMessage({id: "supportPage.supportCards.solutions.4"})}</li>
                            <li>{t.formatMessage({id: "supportPage.supportCards.solutions.5"})}</li>
                            <li>{t.formatMessage({id: "supportPage.supportCards.solutions.6"})}</li>
                        </ul>
                    </div>
                </div>
                <div className={styles.card_wrapper}>
                    <div className={styles.card_container}>
                        <img src={ylBlue} alt="" className={styles.image__ylBlue}/>
                        <span
                            className={styles.card_title}>{t.formatMessage({id: "supportPage.supportCards.videoTutorials.title"})}</span>
                        <ul className={styles.links}>
                            <li>{t.formatMessage({id: "supportPage.supportCards.videoTutorials.1"})}</li>
                            <li>{t.formatMessage({id: "supportPage.supportCards.videoTutorials.2"})}</li>
                            <li>{t.formatMessage({id: "supportPage.supportCards.videoTutorials.3"})}</li>
                            <li>{t.formatMessage({id: "supportPage.supportCards.videoTutorials.4"})}</li>
                        </ul>
                    </div>
                </div>
            </div>
            {
                size.width <= 768 &&
                <div className={styles.help_animation}>
                    <Lottie loop
                            animationData={arrowJson}
                            play
                            className={styles.lottie}
                    />
                </div>
            }
        </div>
    )
}

export default Help;
