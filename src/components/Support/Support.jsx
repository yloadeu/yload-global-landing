import React, {useContext} from "react";
import * as styles from "./Support.module.scss";
import {useIntl} from "gatsby-plugin-intl";
import {navigate} from "gatsby";
import Help from "./Help/Help";
import FastSolutions from "./FastSolutions/FastSolutions";
import Lottie from "react-lottie-player";
import useWindowSize from "../../hooks/useWindowSize";
import {EmailContext} from "../../context/EmailProvider";
import {withPrefix} from "gatsby-link";
import arrowJson from "../../images/animations/arrow.json";
import * as ReactGA from "react-ga";

const Support = () => {
    const t = useIntl();
    const size = useWindowSize();
    const {setEmail} = useContext(EmailContext);
    const linkWithPrefix = withPrefix(`${t.locale}/`);

    const logger = () => {
        ReactGA.event({
            category: 'Contact us',
            action: 'User clicked on "Support" page'
        })
    };

    return (
        <>
            <Help/>
            {/*<FastSolutions/>*/}
            <div className={styles.know_more_container}>
                <div className={styles.question}>
                    <h2>{t.formatMessage({id: "supportPage.knowMore"})}</h2>
                </div>
                <div className={styles.contact}>
                    <Lottie loop
                            animationData={arrowJson}
                            play
                            className={styles.lottie}
                    />
                </div>
                {
                    size.width > 768 ? <button
                            className="button__get_started"
                            id="support_get_started"
                            onClick={() => {
                                logger();
                                navigate(linkWithPrefix + 'contact').then();
                            }}>
                            {t.formatMessage({id: "supportPage.contactUs"})}</button> :
                        <form className="newsletter_form">
                            <input placeholder={t.formatMessage({id: "common.yourEmailPlaceholder"})} type="email"
                                   onChange={(e) => setEmail(e.target.value)}/>
                            <button className="button__get_started"
                                    id="support_get_started"
                                    onClick={() => {
                                        logger();
                                        navigate(linkWithPrefix + 'contact').then();
                                    }}>
                                {t.formatMessage({id: "common.getStartedNow"})}</button>
                        </form>
                }
            </div>
        </>
    )
}

export default Support;
