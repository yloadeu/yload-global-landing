import React from "react";
import {Helmet} from "react-helmet";

const SEOComponent = () => {

    if (typeof document !== `undefined`) {
        const iframes = document.getElementsByTagName("iframe");
        if (iframes.length === 0) {
            const iFrame = document.createElement("iframe");
            iFrame.setAttribute("src", "https://www.googletagmanager.com/ns.html?id=GTM-5XMGQBN")
            iFrame.setAttribute("height", "0")
            iFrame.setAttribute("width", "0")
            iFrame.setAttribute("style", "display:none;visibility:hidden")
            iFrame.innerHTML = ""
            const noScript = document.createElement("noscript");
            noScript.appendChild(iFrame)
            document.body.appendChild(noScript);
        }
        // const image = document.createElement("img");
        // image.setAttribute("src", "https://www.facebook.com/tr?id=1536533576791308&ev=PageView&noscript=1")
        // image.setAttribute("height", "1")
        // image.setAttribute("width", "1")
        // image.setAttribute("style", "display:none;")

        // const noScriptHead = document.createElement("noscript");
        // noScriptHead.appendChild(image)
        // document.head.appendChild(noScriptHead);

        if (document.getElementById('hs-script-loader') === null) {
            const hubspotScript = document.createElement("script");
            hubspotScript.setAttribute("type", "text/javascript");
            hubspotScript.setAttribute("id", "hs-script-loader");
            hubspotScript.setAttribute("async", "");
            hubspotScript.setAttribute("defer", "");
            hubspotScript.setAttribute("src", "//js-eu1.hs-scripts.com/26650886.js");
            hubspotScript.innerHTML = "";
            document.body.appendChild(hubspotScript);
        }
    }

    return (
        <Helmet>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            <meta charSet="UTF-8"/>
            <meta name="facebook-domain-verification" content="qpocd914vrpuxex1jijeu3vljfevyd"/>

            {/* Fonts */}
            <link href="../../fonts/avenir/avenir.css" rel="stylesheet"/>
            <link rel="preconnect" href="https://fonts.googleapis.com"/>
            <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin/>
            <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700&display=swap"
                  rel="stylesheet"/>

            {/* Google Tag Manager  */}
            <script>{`(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-5XMGQBN');`}</script>
            {/* End Google Tag Manager */}

            {/*  Meta Pixel Code */}
            <script>{`
              !function(f,b,e,v,n,t,s)
              {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
              n.callMethod.apply(n,arguments):n.queue.push(arguments)};
              if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
              n.queue=[];t=b.createElement(e);t.async=!0;
              t.src=v;s=b.getElementsByTagName(e)[0];
              s.parentNode.insertBefore(t,s)}(window, document,'script',
              'https://connect.facebook.net/en_US/fbevents.js');
              fbq('init', '1531607933947069');
              fbq('track', 'PageView');
            `}
            </script>
            {/* End Meta Pixel Code */}

        </ Helmet>
    )
}

export default SEOComponent;
