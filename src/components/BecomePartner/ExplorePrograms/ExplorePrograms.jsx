import React from "react";
import * as styles from './ExplorePrograms.module.scss';
import {useIntl} from "gatsby-plugin-intl";

const ExplorePrograms = () => {
    const t = useIntl();
    return (
        <div className={styles.explore_programs_container}>
            <h2 className="page_title_section">{t.formatMessage({id: "becomePartnerPage.explorePrograms.title"})}</h2>
            <h3 className="page_subtitle_section">{t.formatMessage({id: "becomePartnerPage.explorePrograms.subtitle"})}</h3>
            <div className={styles.programs_list}>
                <div className={styles.program + " " + styles.program__channel}>
                    <h5>{t.formatMessage({id: "becomePartnerPage.explorePrograms.channel.title"})}</h5>
                    <p>{t.formatMessage({id: "becomePartnerPage.explorePrograms.channel.text"})}</p>
                </div>
                <div className={styles.program + " " + styles.program__referral}>
                    <h5>{t.formatMessage({id: "becomePartnerPage.explorePrograms.referral.title"})}</h5>
                    <p>{t.formatMessage({id: "becomePartnerPage.explorePrograms.referral.text"})}</p>
                </div>
                <div className={styles.program + " " + styles.program__tech}>
                    <h5>{t.formatMessage({id: "becomePartnerPage.explorePrograms.tech.title"})}</h5>
                    <p>{t.formatMessage({id: "becomePartnerPage.explorePrograms.tech.text"})}</p>
                </div>
                <div className={styles.program + " " + styles.program__alliances}>
                    <h5>{t.formatMessage({id: "becomePartnerPage.explorePrograms.alliances.title"})}</h5>
                    <p>{t.formatMessage({id: "becomePartnerPage.explorePrograms.alliances.text"})}</p>
                </div>
                <div className={styles.program + " " + styles.program__academic}>
                    <h5>{t.formatMessage({id: "becomePartnerPage.explorePrograms.academic.title"})}</h5>
                    <p>{t.formatMessage({id: "becomePartnerPage.explorePrograms.academic.text"})}</p>
                </div>
            </div>
            <h3 className="page_subtitle_section">{t.formatMessage({id: "becomePartnerPage.join"})}</h3>
            <button className="button__get_started" id="explore_programs_apply_now"
                    onClick={() => window.open('https://form.typeform.com/to/huLzHmz0')}>{t.formatMessage({id: "becomePartnerPage.applyNow"})}</button>
        </div>
    )
}

export default ExplorePrograms;
