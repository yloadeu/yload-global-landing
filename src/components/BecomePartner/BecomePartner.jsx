import React from "react";
import PartnerHeader from "./PartnerHeader/PartnerHeader";
import ExplorePrograms from "./ExplorePrograms/ExplorePrograms";

const BecomePartner = () => {
    return (
        <>
            <PartnerHeader/>
            <ExplorePrograms/>
        </>
    )
}

export default BecomePartner;
