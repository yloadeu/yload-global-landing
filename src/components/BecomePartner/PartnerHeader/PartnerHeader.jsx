import React, {useEffect, useState} from 'react';
import * as styles from "./PartnerHeader.module.scss";
import {useIntl} from "gatsby-plugin-intl";
import partnerBackground from "../../../images/header_partner.svg"
import useWindowSize from "../../../hooks/useWindowSize";

const PartnerHeader = () => {
    const t = useIntl();
    const size = useWindowSize();
    const [overlayWidth, setOverlayWidth] = useState(0);

    useEffect(() => {
        setOverlayWidth((size.width - 2000)/2 + 30);
    })

    return (
        <div className={styles.partner_header_wrapper}>
            <div className={styles.partner_header_container}>
                <div className={styles.partner_header_text}>
                    <h1 className="page_title_section">{t.formatMessage({id: "becomePartnerPage.title"})}</h1>
                    <p>{t.formatMessage({id: "becomePartnerPage.description"})}</p>
                    <button className="button__get_started" id="partner_apply_now"
                            onClick={() => window.open('https://form.typeform.com/to/huLzHmz0')}>
                        {t.formatMessage({id: "becomePartnerPage.applyNow"})}</button>
                </div>
                <div className={styles.partner_header_image}>
                    <img src={partnerBackground} alt=""/>
                </div>
            </div>
            <div className={styles.partner_header_overlay} style={{ '--width': overlayWidth +'px'}}/>
        </div>
    )
}

export default PartnerHeader;
