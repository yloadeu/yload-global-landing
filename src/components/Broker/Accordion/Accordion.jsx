import React, {useState} from "react";
import AccordionItem from "./AccordionItem/AccordionItem";
import * as styles from './Accordion.module.scss'
import {useIntl} from "gatsby-plugin-intl";

const Accordion = () => {
    const t = useIntl();
    const [active, setActive] = useState(null);

    const faqs = [
        {
            id: 1,
            header: t.formatMessage({id: "broker.faq.q1.title"}),
            text: t.formatMessage({id: "broker.faq.q1.description"}),
        },
    ];


    const handleToggle = (index) => {
        if (active === index) {
            setActive(null);
        } else {
            setActive(index);
        }
    };

    return (
        <article className={styles.accordion_container}>
            <h3 className="page_title_section"> {t.formatMessage({id: "broker.faq.title"})}</h3>

            {faqs.map((faq, index) => {
                return (
                    <AccordionItem
                        key={index}
                        active={active}
                        handleToggle={handleToggle}
                        faq={faq}
                    />
                );
            })}
        </article>
    );
};

export default Accordion;