import React, {useRef} from "react";
import arrowDown from '../../../../images/down-arrow.svg'
import * as styles from '../Accordion.module.scss'

const AccordionItem = (props) => {
    const contentEl = useRef();
    const {handleToggle, active, faq} = props;
    const {header, id, text} = faq;

    return (
        <div className={styles.accordion_item}>
            <header
                className={active === id ? "active" : ""}
                onClick={() => handleToggle(id)}
            >
                <h2>{header}</h2>
                <img className={styles.arrow} src={arrowDown} alt=''/>
            </header>
            <div
                ref={contentEl}
                className={`collapse ${active === id ? "show" : ""}`}
                style={
                    active === id
                        ? {height: contentEl?.current?.scrollHeight}
                        : {height: "0px"}
                }
            >
                <p>{text}</p>
            </div>
        </div>
    );
};

export default AccordionItem;