import React from "react";
import * as styles from './ApplicationSection.module.scss';
import Lottie from "react-lottie-player";
import arrowJson from "../../../images/animations/arrow.json";
import {useIntl} from "gatsby-plugin-intl";
import useWindowSize from "../../../hooks/useWindowSize";
import ReactGA from "react-ga4";

const ApplicationSection = () => {
    const t = useIntl();
    const size = useWindowSize();

    ReactGA.initialize('G-5H08BDB73G');

    const logger = () => {
        ReactGA.event({
            category: 'Broker Partnership Program',
            action: 'User clicked on the "Apply" button'
        })
    };

    return (
        <div className={styles.apply_guide}>
            <div className={styles.question}>
                <h2>{t.formatMessage({id: "broker.applyGuide.description"})}</h2>
            </div>
            <div className={styles.contact}>
                <Lottie loop
                        animationData={arrowJson}
                        play
                        className={styles.lottie}
                />
            </div>
            {
                size.width > 768 ? <button
                        className={`button__get_started ${styles.button_apply}`}
                        id="support_get_started"
                        onClick={() => {
                            logger();
                            window.open('https://26650886.hs-sites-eu1.com/form-angajare')
                        }}>
                        {t.formatMessage({id: "broker.applyGuide.button"})}</button> :
                    <button className={`button__get_started ${styles.button_apply}`}
                            id="support_get_started"
                            onClick={() => {
                                logger();
                                window.open('https://26650886.hs-sites-eu1.com/form-angajare')
                            }}>
                        {t.formatMessage({id: "broker.applyGuide.button"})}</button>
            }
        </div>
    )

}

export default ApplicationSection;