import React from "react";
import * as styles from './BrokerCards.module.scss'
import {useIntl} from "gatsby-plugin-intl";
import useWindowSize from "../../../hooks/useWindowSize";
import ylYellow from "../../../images/support/ylYellow.svg";
import ylRed from "../../../images/support/ylRed.svg";
import ylLime from "../../../images/support/ylLime.svg";
import ylBlue from "../../../images/support/ylBlue.svg";

const BrokerCards = () => {
    const t = useIntl();
    const size = useWindowSize();

    return (

            <div className={styles.cards_row}>
                <div className={styles.card_wrapper}>

                    <div className={styles.card_container}>
                        <img src={ylBlue} alt="" className={styles.image__ylBlue}/>
                        <img src={ylLime} alt="" className={styles.image__ylLime}/>
                        <span
                            className={styles.card_title}>{t.formatMessage({id: "broker.brokerProgram.title"})}</span>
                        <ul className={styles.links}>
                            <li>{t.formatMessage({id: "broker.brokerProgram.1"})}</li>
                            <li> {t.formatMessage({id: "broker.brokerProgram.2"})}</li>
                            <li> {t.formatMessage({id: "broker.brokerProgram.3"})}</li>
                            <li> {t.formatMessage({id: "broker.brokerProgram.4"})}</li>
                            <li> {t.formatMessage({id: "broker.brokerProgram.5"})}</li>
                        </ul>
                    </div>
                </div>
                <div className={styles.card_wrapper}>
                    <div className={styles.card_container}>
                        <img src={ylRed} alt="" className={styles.image__ylRed}/>
                        <span
                            className={styles.card_title}>{t.formatMessage({id: "broker.brokerBenefits.title"})}</span>
                        <ul>
                            <li> {t.formatMessage({id: "broker.brokerBenefits.1"})}</li>
                            <li> {t.formatMessage({id: "broker.brokerBenefits.2"})}</li>
                            <li> {t.formatMessage({id: "broker.brokerBenefits.3"})}</li>
                            <li> {t.formatMessage({id: "broker.brokerBenefits.4"})}</li>
                            <li> {t.formatMessage({id: "broker.brokerBenefits.5"})}</li>
                            <li> {t.formatMessage({id: "broker.brokerBenefits.6"})}</li>
                        </ul>
                    </div>
                </div>
                <div className={styles.card_wrapper}>
                    <div className={styles.card_container}>
                        <img src={ylYellow} alt="" className={styles.image__ylYellow}/>
                        <span
                            className={styles.card_title}>{t.formatMessage({id: "broker.brokerExpectations.title"})}</span>
                        <ul className={styles.links}>
                            <li> {t.formatMessage({id: "broker.brokerExpectations.1"})}</li>
                            <li> {t.formatMessage({id: "broker.brokerExpectations.2"})}</li>
                            <li> {t.formatMessage({id: "broker.brokerExpectations.3"})}</li>
                            <li> {t.formatMessage({id: "broker.brokerExpectations.4"})}</li>
                        </ul>
                    </div>
                </div>
            </div>
    )

}

export default BrokerCards;