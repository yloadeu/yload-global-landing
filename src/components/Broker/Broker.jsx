import React from "react";
import * as styles from './Broker.module.scss'
import ApplicationSection from "./ApplicationSection/ApplicationSection";
import BrokerCards from "./BrokerCards/BrokerCards";
import {useIntl} from "gatsby-plugin-intl";
import Contact from "./Contact/Contact";
import Accordion from "./Accordion/Accordion";
import ReactGA from 'react-ga4';

const Broker = () => {
    const t = useIntl();

    ReactGA.initialize('G-5H08BDB73G');

    const logger = () => {
        ReactGA.event({
            category: 'Broker Partnership Program',
            action: 'User clicked on "Join the YLOAD Broker Partnership Program" button'
        })
    };

    const questionsAnswers = [
        {
            question: "How many team members can I invite?",
            answer:
                "You can invite up to 2 additional users on the Free plan. There is no limit on team members for the Premium plan.",
        },
        {
            question: "What is the maximum file upload size?",
            answer:
                "No more than 2GB. All files in your account must fit your allotted storage space.",
        },
        {
            question: "How do I reset my password?",
            answer: `Click “Forgot password” from the login page or “Change password” from your profile page. A reset link will be emailed to you.`,
        },
        {
            question: "Can I cancel my subscription?",
            answer: `Yes! Send us a message and we’ll process your request no questions asked.`,
        },
        {
            question: "Do you provide additional support?",
            answer: `Chat and email support is available 24/7. Phone lines are open during normal business hours.`,
        },
    ];

    return (
        <>
            <div className={styles.broker_container}>
                <div className={styles.broker_subtitle_container}>
                    <h1 className="page_title_section">{t.formatMessage({id: "broker.title"})}</h1>
                    <h3 className="page_subtitle_section">{t.formatMessage({id: "broker.subtitle"})}</h3>
                    <button className="button__get_started" id="help_find_out"
                            onClick={() => {
                                logger();
                                window.open('https://26650886.hs-sites-eu1.com/form-angajare')
                            }}>
                        {t.formatMessage({id: "broker.buttonText"})}
                    </button>
                </div>
            </div>
            <BrokerCards/>
            <ApplicationSection/>
            <Accordion/>

            <Contact/>
        </>
    )

}

export default Broker;