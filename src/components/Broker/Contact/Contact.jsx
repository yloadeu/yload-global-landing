import React from 'react';
import * as styles from "./Contact.module.scss";
import {useIntl} from "gatsby-plugin-intl";

const Contact = () => {
    const t = useIntl();

    return (
        <>
            <div className={styles.broker_contact_container}>
                <h3 className="page_subtitle_section">{t.formatMessage({id: "broker.contact.description.p1"})}
                    <strong> {t.formatMessage({id: "broker.contact.description.email"})}</strong> {t.formatMessage({id: "broker.contact.description.p2"})}
                    <strong> {t.formatMessage({id: "broker.contact.description.phone"})}</strong>
                </h3>
                <h3 className="page_subtitle_section">{t.formatMessage({id: "broker.contact.description.p3"})}
                </h3>
            </div>
        </>
    )
}

export default Contact;