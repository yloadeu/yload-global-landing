import React from 'react';
import * as styles from './Support.module.scss'
import Lottie from 'react-lottie-player'
import {useIntl} from 'gatsby-plugin-intl'
import lottieJson from '../../../images/animations/28306.json'
import * as ReactGA from "react-ga";

const Support = () => {
    const t = useIntl();

    const logger = () => {
        ReactGA.event({
            category: 'Call us now',
            action: 'User clicked on "Real-Time Support" section'
        })
    };

    return (
        <div className={styles.support_container} id="support">
            <div className={styles.support_text}>
                <h2 className="page_title_section">{t.formatMessage({id: "support.title"})}</h2>
                <p>{t.formatMessage({id: "support.description"})}</p>
                <p><a href="tel:+40377101515" onClick={() => logger()} id="support_call_us">+40 377 101 515</a> - {t.formatMessage({id: "support.callUs"})}</p>
            </div>

            <div className={styles.support_animation}>
                <Lottie loop
                        animationData={lottieJson}
                        play
                        className={styles.lottie}
                />
            </div>
        </div>
    )
}

export default Support
