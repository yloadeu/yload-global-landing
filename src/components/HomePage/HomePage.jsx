import React from "react";
import Header from "./Header/Header";
import Future from "./Future/Future";
import Benefits from "./Benefits/Benefits";
import SaveTime from "./SaveTime/SaveTime";
import Support from "./Support/Support";
import Brands from "./Brands/Brands";
import Newsletter from "./Newsletter/Newsletter";
import Tabs from "./Tabs/Tabs";

const HomePage = () => {
    return (
        <>
            <Header/>
            <Future/>
            <Benefits/>
            <SaveTime/>
            <Tabs/>
            <Support/>
            <Brands/>
            <Newsletter/>
        </>
    )
}

export default HomePage;
