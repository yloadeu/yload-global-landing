import React from "react";
import * as styles from './ScreensStack.module.scss';
import {motion} from "framer-motion";
import headerScreen1 from '../../../../images/home/header/header-screen-1.webp';
import headerScreen2 from '../../../../images/home/header/header-screen-3.webp';
import headerScreen3 from '../../../../images/home/header/header-screen-2.webp';
import move from "lodash-move";

const CARD_SCREENS = [headerScreen1, headerScreen2, headerScreen3];
const CARD_OFFSET = 100;
const SCALE_FACTOR = 0.05;

const ScreensStack = () => {
    const [cards, setCards] = React.useState(CARD_SCREENS);
    const moveToEnd = (from) => {
        setCards(move(cards, from, cards.length - 1));
    };

    return (
        <div style={wrapperStyle} className={styles.screens_container}>
            <ul style={cardWrapStyle} className={styles.screens}>
                {cards.map((url, index) => {
                    const canDrag = index === 0;

                    return (
                        <motion.li
                            key={url}
                            style={{
                                ...cardStyle,
                                backgroundImage: `url(${url})`,
                                backgroundSize: '100%, 100%, cover',
                                backgroundRepeat: 'no-repeat',
                                // cursor: canDrag ? "grab" : "auto",
                                // animation:  `${styles.changeZIndex} 6s infinite ease`,
                                // animationDelay: index === 1 ? `${index + 3}s` : index === 0 ? `${index + 6}s` : `${index + 8}s` ,
                                // '--index': index === 1 ? 2 : index === 0 ? 1 : 3,
                            }}
                            animate={{
                                top: index === 1 ? 120 : (index * -CARD_OFFSET + index * 30),
                                right: index === 1 ? -50 : index === 0 ? 20 : 130,
                                scale: 1 - index * SCALE_FACTOR,
                                zIndex: CARD_SCREENS.length - index
                            }}

                            // drag={canDrag ? "y" : false}
                            dragConstraints={{
                                top: 0,
                                right: 0,
                                left: 0,
                                bottom: 0
                            }}
                            // onDragEnd={() => moveToEnd(index)}
                            // onClick={() => moveToEnd(index)}
                        />
                    );
                })}
            </ul>
        </div>
    );
};
const wrapperStyle = {
    position: "relative",
    display: "flex",
    alignItems: "center",
    height: "850px",
    width: "100%"
};

const cardWrapStyle = {
    position: "relative",
    maxWidth: "1000px",
    width: "100%",
    maxHeight: "590px",
    height: "100%"
};

const cardStyle = {
    position: "absolute",
    maxWidth: "800px",
    width: "100%",
    maxHeight: "590px",
    height: "100%",
    borderRadius: "8px",
    transformOrigin: "top center",
    listStyle: "none"
};

export default ScreensStack;
