import React from "react";
import * as styles from './Header.module.scss';
import {useIntl} from "gatsby-plugin-intl";
import { graphql, useStaticQuery} from 'gatsby';
import Image from "gatsby-image"
import useWindowSize from "../../../hooks/useWindowSize";
import ScreensStack from "./ScreensStack/ScreensStack";
import * as ReactGA from "react-ga";

export const getScreens = graphql`
  {
    screens: file(relativePath: { eq: "home/header/header-screens.webp" }) {
      childImageSharp { 
        fluid(quality: 100, webpQuality: 100, maxWidth: 6080) {
          ...GatsbyImageSharpFluid_noBase64
        }
      }
    }
    
    mobileScreens: file(relativePath: { eq: "home/header/mobile-screens-1.webp" }) {
      childImageSharp { 
        fluid(quality: 100, webpQuality: 100, maxWidth: 6080) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

const Header = () => {
    const t = useIntl();
    const size = useWindowSize();

    const {
        screens,
        mobileScreens
    } = useStaticQuery(getScreens)

    const logger = () => {
        ReactGA.event({
            category: 'Get started now',
            action: 'User clicked on "1st European complete logistics system" section'
        })
    };

    return (
        <>
            <div className={styles.header_container}>
                <div className={styles.header_content}>
                    <div className={styles.header_text_section}>
                        <h1 className="page_title_section">{t.formatMessage({id: "header.title"})}</h1>
                        <h3 className="page_subtitle_section">{t.formatMessage({id: "header.subtitle"})}</h3>
                        <input placeholder={t.formatMessage({id: "header.workEmailPlaceholder"})} type="email"/>
                        <div className={styles.button_section}>
                            <button className="button__get_started"
                                    id="header_get_started"
                                    onClick={() => {
                                        logger();
                                        t.locale === 'ro' ? window.open('https://share-eu1.hsforms.com/1QBYJOzIWRNCOOQomhqKBGAfv7ye') :
                                            window.open('https://share-eu1.hsforms.com/1s_SB9dP2Qjm6ZHHiTE7U3Qfv7ye')
                                    }}>
                                {t.formatMessage({id: "common.getStartedNow"})}
                            </button>
                            <h6>{t.formatMessage({id: "header.freePeriod"})}
                                <br/>{t.formatMessage({id: "header.creditCard"})}
                            </h6>
                        </div>
                    </div>
                    <div className={styles.header_screens}>
                        {
                            size.width <= 768 ?
                                <Image fluid={mobileScreens.childImageSharp.fluid} alt="header-screens"/> :
                                size.width <= 1200 ?
                                    <Image fluid={screens.childImageSharp.fluid} alt="header-screens"/> :
                                    <ScreensStack/>
                        }
                    </div>
                </div>
            </div>
        </>
    )
}

export default Header;
