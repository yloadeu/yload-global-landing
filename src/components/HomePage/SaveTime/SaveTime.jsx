import React from "react";
import * as styles from "./SaveTime.module.scss";
import {useIntl} from 'gatsby-plugin-intl';
import useWindowSize from "../../../hooks/useWindowSize";
import advantageImage from "../../../images/home/save-time/save-time.webp";
import advantageImageMobile from "../../../images/home/save-time/save-time-mobile.webp";
import iconFileText from "../../../images/home/save-time/icon-feather-file-text.svg";
import iconMail from "../../../images/home/save-time/icon-feather-mail.svg";
import iconPhone from "../../../images/home/save-time/icon-feather-phone.svg";
import * as ReactGA from "react-ga";

const SaveTime = () => {
    const t = useIntl();
    const size = useWindowSize();

    const logger = () => {
        ReactGA.event({
            category: 'Get started now',
            action: 'User clicked on "Save time and money with YLOAD" section'
        })
    };

    return (
        <div className={styles.save_time_wrapper} id="save-time">
            <div className={styles.save_time_content}>
                <span style={{textTransform: "uppercase"}}>{t.formatMessage({id: "saveTime.builtForEveryone"})}</span>
                <h2 className="page_title_section">{t.formatMessage({id: "saveTime.title"})}</h2>

                <div className={styles.advantages}>
                    <div className="advantages_text">
                        <div className="advantage">
                            <div className="advantage_icon_background">
                                <div><img src={iconMail} alt="icon-mail"/></div>
                            </div>
                            <div className="advantage_info">
                                <h4>{t.formatMessage({id: "saveTime.declutterOffice.title"})}</h4>
                                <p>{t.formatMessage({id: "saveTime.declutterOffice.description"})}</p>
                            </div>
                        </div>
                        <div className="advantage">
                            <div className="advantage_icon_background">
                                <div><img src={iconFileText} alt="icon-file-text"/></div>
                            </div>
                            <div className="advantage_info">
                                <h4>{t.formatMessage({id: "saveTime.savePaper.title"})}</h4>
                                <p>{t.formatMessage({id: "saveTime.savePaper.description"})}</p>
                            </div>
                        </div>
                        <div className="advantage">
                            <div className="advantage_icon_background">
                                <div><img src={iconPhone} alt="icon-phone"/></div>
                            </div>
                            <div className="advantage_info">
                                <h4>{t.formatMessage({id: "saveTime.endlessCalls.title"})}</h4>
                                <p>{t.formatMessage({id: "saveTime.endlessCalls.description"})}</p>
                            </div>
                        </div>
                        {
                            size.width > 1200 && <button className="button__get_started"
                                                         id="save_time_get_started"
                                                         onClick={() => {
                                                             logger();
                                                             t.locale === 'ro' ? window.open('https://share-eu1.hsforms.com/1QBYJOzIWRNCOOQomhqKBGAfv7ye') :
                                                                 window.open('https://share-eu1.hsforms.com/1s_SB9dP2Qjm6ZHHiTE7U3Qfv7ye')
                                                         }}>
                                {/*onClick={() => navigate("/preregister/")}>*/}
                                {t.formatMessage({id: "common.getStartedNow"})}</button>
                        }
                    </div>
                    <div className={styles.advantages_image}>
                        {
                            size.width > 1200 ?
                                <img src={advantageImage} alt="save-time-and-money"/> :
                                <img src={advantageImageMobile} alt="save-time-and-money"/>
                        }
                    </div>
                    {
                        size.width <= 1200 && <button className="button__get_started" id="save_time_get_started"
                                                      onClick={() => {
                                                          logger();
                                                          t.locale === 'ro' ? window.open('https://share-eu1.hsforms.com/1QBYJOzIWRNCOOQomhqKBGAfv7ye') :
                                                              window.open('https://share-eu1.hsforms.com/1s_SB9dP2Qjm6ZHHiTE7U3Qfv7ye')
                                                      }}>
                            {/*onClick={() => navigate("/preregister/")}>*/}
                            {t.formatMessage({id: "common.getStartedNow"})}</button>
                    }
                </div>
            </div>
        </div>
    )
}

export default SaveTime
