import React from 'react';
import * as styles from "./Newsletter.module.scss"
import {useIntl} from 'gatsby-plugin-intl'
import * as ReactGA from "react-ga";

const Newsletter = () => {
    const t = useIntl();

    const logger = () => {
        ReactGA.event({
            category: 'Get started now',
            action: 'User clicked on "Optimise your transport - Newsletter" section'
        })
    };

    return (
        <>
            <div className={styles.offer_container}>
                <h2 className="page_title_section">{t.formatMessage({id: "newsletter.title"})}
                    <strong> {t.formatMessage({id: "newsletter.YN"})}</strong></h2>
                <h3 className="page_subtitle_section">{t.formatMessage({id: "newsletter.freePeriod"})}</h3>
                <button className="button__get_started"
                        id="newsletter_get_started"
                        onClick={() => {
                            logger();
                            t.locale === 'ro' ? window.open('https://share-eu1.hsforms.com/1QBYJOzIWRNCOOQomhqKBGAfv7ye') :
                                window.open('https://share-eu1.hsforms.com/1s_SB9dP2Qjm6ZHHiTE7U3Qfv7ye')
                        }}>
                    {t.formatMessage({id: "common.getStartedNow"})}
                </button>
            </div>
        </>
    )
}

export default Newsletter
