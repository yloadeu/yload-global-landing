import React from "react";
import * as styles from './Benefits.module.scss';
import useWindowSize from "../../../hooks/useWindowSize";
import benefitsGroup from "../../../images/home/benefits/yload-carrier-image.webp";
import benefitsGroupMobile from "../../../images/home/benefits/yload-carrier-image-mobile.webp";
import Parallax from "react-rellax";
import BenefitsContent from "./BenefitsContent";
import {useStaticQuery} from "gatsby";

const Benefits = () => {
    const size = useWindowSize();

    return (
        <div className={styles.benefits_container} id="why-yload">

            <div className={styles.benefits}>

                {
                    size.width > 1200 &&
                    <Parallax speed={7} percentage={0} className={styles.parallax_container}>
                        <BenefitsContent/>
                    </Parallax>
                }
                {
                    size.width <= 1200 && <BenefitsContent/>
                }

                <div className={styles.benefits_images}>
                    {
                        size.width > 1200 && <img src={benefitsGroup} alt="benefits"/>
                    }
                    {
                        size.width <= 1200 && <img src={benefitsGroupMobile} alt="benefits"/>
                    }
                </div>
            </div>

        </div>
    )
}

export default Benefits
