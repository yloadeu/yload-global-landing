import React from "react";
import * as styles from './Benefits.module.scss';
import {useIntl} from 'gatsby-plugin-intl'
import accountIcon from "../../../images/home/benefits/account.svg";
import locationIcon from "../../../images/home/benefits/location.svg";
import fleetManagementIcon from "../../../images/home/benefits/fleet-management.svg";
import dynamicLoadIcon from "../../../images/home/benefits/dynamic-load.svg";
import cargoIcon from "../../../images/home/benefits/cargo.svg";
import liveBudgetIcon from "../../../images/home/benefits/live-budget.svg";
import reportsIcon from "../../../images/home/benefits/reports.svg";
import companiesLogo from "../../../images/home/benefits/companies.svg";

const BenefitsContent = () => {
    const t = useIntl();

    return (
        <div className={styles.benefits_text}>
            <h2 className="page_title_section">{t.formatMessage({id: "whyYload.title"})}</h2>

            <div className={styles.benefits_category}>
                <h3 className="page_subtitle_section">{t.formatMessage({id: "whyYload.yloadCarriers.title"})}</h3>
                <div className={styles.benefit}>
                    <div><img src={accountIcon} alt=""/></div>
                    <span>{t.formatMessage({id: "whyYload.yloadCarriers.account"})}</span>
                </div>

                <div className={styles.benefit}>
                    <div><img src={locationIcon} alt=""/></div>
                    <span>{t.formatMessage({id: "whyYload.yloadCarriers.location"})}</span>
                </div>

                <div className={styles.benefit}>
                    <div><img src={fleetManagementIcon} alt=""/></div>
                    <span>{t.formatMessage({id: "whyYload.yloadCarriers.fleetManagement"})}</span>
                </div>

                <div className={styles.benefit}>
                    <div><img src={dynamicLoadIcon} alt=""/></div>
                    <span>{t.formatMessage({id: "whyYload.yloadCarriers.dynamicLoad"})}</span>
                </div>

                <div className={styles.benefit}>
                    <div><img src={cargoIcon} alt=""/></div>
                    <span>{t.formatMessage({id: "whyYload.yloadCarriers.unlimitedFreight"})}</span>
                </div>
            </div>
            <div className={styles.benefits_category}>

                <h3 className="page_subtitle_section">{t.formatMessage({id: "whyYload.yloadNetwork.title"})}</h3>

                <div className={styles.benefit}>
                    <div><img src={liveBudgetIcon} alt=""/></div>
                    <span>{t.formatMessage({id: "whyYload.yloadNetwork.liveBudget"})}</span>
                </div>

                <div className={styles.benefit}>
                    <div><img src={reportsIcon} alt=""/></div>
                    <span>{t.formatMessage({id: "whyYload.yloadNetwork.financial"})}</span>
                </div>

                <div className={styles.benefit}>
                    <div><img src={companiesLogo} alt=""/></div>
                    <span>{t.formatMessage({id: "whyYload.yloadNetwork.companies"})}</span>
                </div>
            </div>
        </div>
    )
}

export default BenefitsContent
