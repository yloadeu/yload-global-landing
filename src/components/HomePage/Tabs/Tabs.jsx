import React from 'react';
import Truckers from "./Truckers/Truckers";
import Network from "./Network/Network";
import Express from "./Express/Express";

const Tabs = () => {
    return (
        <>
            <Truckers/>
            <Network/>
            <Express/>
        </>
    )
}

export default Tabs
