import React, {useState} from 'react';
import * as styles from '../Tabs.module.scss';
// import logistics from '../../../../images/home/tabs/logistics.webp';
import {useIntl} from 'gatsby-plugin-intl'
import {graphql, useStaticQuery} from "gatsby";
import Image from "gatsby-image";
import * as ReactGA from "react-ga";

export const getImages = graphql`
  {
    logistics: file(relativePath: { eq: "home/tabs/logistics.webp" }) {
      childImageSharp { 
        fluid(quality: 100, webpQuality: 100, maxWidth: 2080) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

const Network = () => {
    const t = useIntl();
    const [active, setActive] = useState(0);
    const handleClick = e => {
        const index = parseInt(e.target.id, 0);
        if (index !== active) {
            setActive(index);
        }
    };

    const {
        logistics
    } = useStaticQuery(getImages)

    const logger = () => {
        ReactGA.event({
            category: 'Get started now',
            action: 'User clicked on "YNetwork" section'
        })
    };

    return (
        <div className={styles.tabs_container + " " + styles.tabs_container__network}>
            <h2 className="page_title_section">{t.formatMessage({id: "networkCard.yloadNetwork.tabName"})}</h2>
            <p className="page_subtitle_section">{t.formatMessage({id: "networkCard.yloadNetwork.tabDescription"})}</p>
            <div className={styles.tabs}>
                <div className={styles.tab_text}>
                    <div className={styles.tabs_grid}>
                        <div onClick={(e) => handleClick(e)} id={0}
                             className={active === 0 ? `${styles.active_tab}` : ""}>
                            {t.formatMessage({id: "networkCard.visibility.tabName"})}
                        </div>
                        <div onClick={(e) => handleClick(e)} id={1}
                             className={active === 1 ? `${styles.active_tab}` : ""}>
                            {t.formatMessage({id: "networkCard.costReduction.tabName"})}
                        </div>
                        <div onClick={(e) => handleClick(e)} id={2}
                             className={active === 2 ? `${styles.active_tab}` : ""}>
                            {t.formatMessage({id: "networkCard.teamManagement.tabName"})}
                        </div>
                        <div onClick={(e) => handleClick(e)} id={3}
                             className={active === 3 ? `${styles.active_tab}` : ""}>
                            {t.formatMessage({id: "networkCard.cloudDocs.tabName"})}
                        </div>
                        <div onClick={(e) => handleClick(e)} id={4}
                             className={active === 4 ? `${styles.active_tab}` : ""}>
                            {t.formatMessage({id: "networkCard.erp.tabName"})}
                        </div>
                    </div>
                    <div className={styles.tab_content}>
                        {
                            active === 0 && <>
                                <h2 className="page_title_section">{t.formatMessage({id: "networkCard.visibility.tabTitle"})}</h2>
                                <p>{t.formatMessage({id: "networkCard.visibility.tabText"})}</p>
                            </>
                        }
                        {
                            active === 1 && <>
                                <h2 className="page_title_section">{t.formatMessage({id: "networkCard.costReduction.tabTitle"})}</h2>
                                <p>{t.formatMessage({id: "networkCard.costReduction.tabText"})}</p>
                            </>
                        }
                        {
                            active === 2 &&
                            <>
                                <h2 className="page_title_section">{t.formatMessage({id: "networkCard.teamManagement.tabTitle"})}</h2>
                                <p>{t.formatMessage({id: "networkCard.teamManagement.tabText"})}</p>
                            </>
                        }
                        {
                            active === 3 && <>
                                <h2 className="page_title_section">{t.formatMessage({id: "networkCard.cloudDocs.tabTitle"})}</h2>
                                <p>{t.formatMessage({id: "networkCard.cloudDocs.tabText"})}</p>
                            </>
                        }
                        {
                            active === 4 && <>
                                <h2 className="page_title_section">{t.formatMessage({id: "networkCard.erp.tabTitle"})}</h2>
                                <p>{t.formatMessage({id: "networkCard.erp.tabText"})}</p>
                            </>
                        }
                    </div>
                </div>

                <div className={styles.tab_image}>
                    {
                        active === 0 && <Image fluid={logistics.childImageSharp.fluid} alt="visibility"/>
                    }
                    {
                        active === 1 && <Image fluid={logistics.childImageSharp.fluid} alt="cost-reduction"/>
                    }
                    {
                        active === 2 && <Image fluid={logistics.childImageSharp.fluid} alt="team-management"/>
                    }
                    {
                        active === 3 && <Image fluid={logistics.childImageSharp.fluid} alt="cloud-docs"/>
                    }
                    {
                        active === 4 && <Image fluid={logistics.childImageSharp.fluid} alt="ERP"/>
                    }
                </div>
                <button className={`${styles.tab_button + " button__get_started"}`}
                        id="network_get_started"
                        onClick={() => {
                            logger();
                            t.locale === 'ro' ? window.open('https://share-eu1.hsforms.com/1WOggoIkpSEKbN5j9IAufZQfv7ye') :
                                window.open('https://share-eu1.hsforms.com/1jEhrhVCQQES1B_bPATuZ8Afv7ye')
                        }}>
                    {t.formatMessage({id: "common.getStartedNow"})}</button>
            </div>
        </div>
    )
}

export default Network;
