import React, {useState} from 'react';
import * as styles from '../Tabs.module.scss';
import {useIntl} from 'gatsby-plugin-intl'
import {graphql, useStaticQuery} from "gatsby";
import Image from "gatsby-image";
import * as ReactGA from "react-ga";

export const getImages = graphql`
  {
    tracking: file(relativePath: { eq: "home/tabs/tracking.webp" }) {
      childImageSharp { 
        fluid(quality: 100, webpQuality: 100, maxWidth: 2080) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

const Truckers = () => {
    const t = useIntl();
    const [active, setActive] = useState(0);
    const handleClick = e => {
        const index = parseInt(e.target.id, 0);
        if (index !== active) {
            setActive(index);
        }
    };

    const {
        tracking
    } = useStaticQuery(getImages)

    const logger = (isLink) => {
        isLink ? ReactGA.event({
                category: 'Check now how tracking works on YLOAD',
                action: 'User clicked on link'
            }) :
            ReactGA.event({
                category: 'Get started now',
                action: 'User clicked on "YTrucks" section'
            })
    };

    return (
        <div className={styles.tabs_container + " " + styles.tabs_container__tracking} id="product">
            <h2 className="page_title_section">{t.formatMessage({id: "truckersCard.yloadTruckers.tabName"})}</h2>
            <p className="page_subtitle_section">{t.formatMessage({id: "truckersCard.yloadTruckers.tabText"})}</p>
            <div className={styles.tabs}>
                <div className={styles.tab_text}>
                    <div className={styles.tabs_grid}>
                        <div onClick={(e) => handleClick(e)} id={0}
                             className={active === 0 ? `${styles.active_tab}` : ""}>
                            {t.formatMessage({id: "truckersCard.fleetManagement.tabName"})}
                        </div>
                        <div onClick={(e) => handleClick(e)} id={1}
                             className={active === 1 ? `${styles.active_tab}` : ""}>
                            {t.formatMessage({id: "truckersCard.wallet.tabName"})}
                        </div>
                        <div onClick={(e) => handleClick(e)} id={2}
                             className={active === 2 ? `${styles.active_tab}` : ""}>
                            {t.formatMessage({id: "truckersCard.driverApp.tabName"})}
                        </div>
                        <div onClick={(e) => handleClick(e)} id={3}
                             className={active === 3 ? `${styles.active_tab}` : ""}>
                            {t.formatMessage({id: "truckersCard.iotSystems.tabName"})}
                        </div>
                        <div onClick={(e) => handleClick(e)} id={4}
                             className={active === 4 ? `${styles.active_tab}` : ""}>
                            {t.formatMessage({id: "truckersCard.telematics.tabName"})}
                        </div>
                        <div onClick={(e) => handleClick(e)} id={5}
                             className={active === 5 ? `${styles.active_tab}` : ""}>
                            {t.formatMessage({id: "truckersCard.ai.tabName"})}
                        </div>
                    </div>
                    <div className={styles.tab_content}>
                        {
                            active === 0 && <>
                                <h2 className="page_title_section">{t.formatMessage({id: "truckersCard.fleetManagement.tabTitle"})}</h2>
                                <p>{t.formatMessage({id: "truckersCard.fleetManagement.tabText"})}</p>
                                <p style={{cursor: 'pointer'}} className={styles.link} id="trucks_tab_link" onClick={() => {
                                    logger(true);
                                    window.open('https://trucks.yload.eu')
                                }}>{t.formatMessage({id: "truckersCard.fleetManagement.tabLink"})}</p>
                            </>
                        }
                        {
                            active === 1 && <>
                                <h2 className="page_title_section">{t.formatMessage({id: "truckersCard.wallet.tabTitle"})}</h2>
                                <p>{t.formatMessage({id: "truckersCard.wallet.tabText"})}</p>
                            </>
                        }
                        {
                            active === 2 && <>
                                <h2 className="page_title_section">{t.formatMessage({id: "truckersCard.driverApp.tabTitle"})}</h2>
                                <p>{t.formatMessage({id: "truckersCard.driverApp.tabText"})}</p>
                            </>
                        }
                        {
                            active === 3 && <>
                                <h2 className="page_title_section">{t.formatMessage({id: "truckersCard.iotSystems.tabTitle"})}</h2>
                                <p>{t.formatMessage({id: "truckersCard.iotSystems.tabText"})}</p>
                            </>
                        }
                        {
                            active === 4 && <>
                                <h2 className="page_title_section">{t.formatMessage({id: "truckersCard.telematics.tabTitle"})}</h2>
                                <p>{t.formatMessage({id: "truckersCard.telematics.tabText"})}</p>
                            </>
                        }
                        {
                            active === 5 && <>
                                <h2 className="page_title_section">{t.formatMessage({id: "truckersCard.ai.tabTitle"})}</h2>
                                <p>{t.formatMessage({id: "truckersCard.ai.tabText"})}</p>
                            </>
                        }
                    </div>
                </div>

                <div className={styles.tab_image}>
                    {
                        active === 0 && <Image fluid={tracking.childImageSharp.fluid} alt="fleetManagement"/>
                    }
                    {
                        active === 1 && <Image fluid={tracking.childImageSharp.fluid} alt="wallet"/>
                    }
                    {
                        active === 2 && <Image fluid={tracking.childImageSharp.fluid} alt="driverApp"/>
                    }
                    {
                        active === 3 && <Image fluid={tracking.childImageSharp.fluid} alt="iotSystems"/>
                    }
                    {
                        active === 4 && <Image fluid={tracking.childImageSharp.fluid} alt="telematics"/>
                    }
                    {
                        active === 5 && <Image fluid={tracking.childImageSharp.fluid} alt="ai"/>
                    }
                </div>
                <button className={`${styles.tab_button + " button__get_started"}`}
                        id="trucks_get_started"
                        onClick={() => {
                            logger(false);
                            t.locale === 'ro' ? window.open('https://share-eu1.hsforms.com/1QBYJOzIWRNCOOQomhqKBGAfv7ye') :
                                window.open('https://share-eu1.hsforms.com/1s_SB9dP2Qjm6ZHHiTE7U3Qfv7ye')
                        }}>
                    {t.formatMessage({id: "common.getStartedNow"})}</button>
            </div>
        </div>
    )
}

export default Truckers;
