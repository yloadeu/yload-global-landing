import React, {useState} from 'react';
import * as styles from '../Tabs.module.scss';
import {graphql, useStaticQuery} from "gatsby";
import {useIntl} from 'gatsby-plugin-intl'
import Image from "gatsby-image";
import * as ReactGA from "react-ga";

export const getImages = graphql`
  {
    liveTracking: file(relativePath: { eq: "home/tabs/live-tracking.png" }) {
      childImageSharp { 
        fluid(quality: 100, webpQuality: 100, maxWidth: 2080) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

const Express = () => {
    const t = useIntl();
    const [active, setActive] = useState(0);
    const handleClick = e => {
        const index = parseInt(e.target.id, 0);
        if (index !== active) {
            setActive(index);
        }
    };

    const {
        liveTracking
    } = useStaticQuery(getImages)

    const logger = () => {
        ReactGA.event({
            category: 'Get started now',
            action: 'User clicked on "YSocial" section'
        })
    };

    return (
        <div className={styles.tabs_container + " " + styles.tabs_container__express}>
            <h2 className="page_title_section">{t.formatMessage({id: "expressCard.yloadExpress.tabName"})}</h2>
            <p className="page_subtitle_section">{t.formatMessage({id: "truckersCard.yloadTruckers.tabText"})}</p>
            <div className={styles.tabs}>
                <div className={styles.tab_text}>
                    <div className={styles.tabs_grid}>
                        <div onClick={(e) => handleClick(e)} id={0}
                             className={active === 0 ? `${styles.active_tab}` : ""}>
                            {t.formatMessage({id: "expressCard.driverSocialNetwork.tabName"})}
                        </div>
                        <div onClick={(e) => handleClick(e)} id={1}
                             className={active === 1 ? `${styles.active_tab}` : ""}>
                            {t.formatMessage({id: "expressCard.discounts.tabName"})}
                        </div>
                        <div onClick={(e) => handleClick(e)} id={2}
                             className={active === 2 ? `${styles.active_tab}` : ""}>
                            {t.formatMessage({id: "expressCard.meetings.tabName"})}
                        </div>
                        <div onClick={(e) => handleClick(e)} id={3}
                             className={active === 3 ? `${styles.active_tab}` : ""}>
                            {t.formatMessage({id: "expressCard.loadManagement.tabName"})}
                        </div>
                        <div onClick={(e) => handleClick(e)} id={4}
                             className={active === 4 ? `${styles.active_tab}` : ""}>
                            {t.formatMessage({id: "expressCard.services.tabName"})}
                        </div>
                    </div>
                    <div className={styles.tab_content}>
                        {
                            active === 0 && <>
                                <h2 className="page_title_section">{t.formatMessage({id: "expressCard.driverSocialNetwork.tabTitle"})}</h2>
                                <p>{t.formatMessage({id: "expressCard.driverSocialNetwork.tabText1"})}</p>
                            </>
                        }
                        {
                            active === 1 && <>
                                <h2 className="page_title_section">{t.formatMessage({id: "expressCard.discounts.tabTitle"})}</h2>
                                <p>{t.formatMessage({id: "expressCard.discounts.tabText"})}</p>
                            </>
                        }
                        {
                            active === 2 && <>
                                <h2 className="page_title_section">{t.formatMessage({id: "expressCard.meetings.tabTitle"})}</h2>
                                <p>{t.formatMessage({id: "expressCard.meetings.tabText"})}</p>
                            </>
                        }
                        {
                            active === 3 &&
                            <>
                                <h2 className="page_title_section">{t.formatMessage({id: "expressCard.loadManagement.tabTitle"})}</h2>
                                <p>{t.formatMessage({id: "expressCard.loadManagement.tabText"})}</p>
                            </>
                        }
                        {
                            active === 4 && <>
                                <h2 className="page_title_section">{t.formatMessage({id: "expressCard.services.tabTitle"})}</h2>
                                <p>{t.formatMessage({id: "expressCard.services.tabText"})}</p>
                            </>
                        }
                    </div>
                </div>

                <div className={styles.tab_image}>
                    {
                        active === 0 && <Image fluid={liveTracking.childImageSharp.fluid} alt="driver-social-network"/>
                    }
                    {
                        active === 1 && <Image fluid={liveTracking.childImageSharp.fluid} alt="discounts"/>
                    }
                    {
                        active === 2 && <Image fluid={liveTracking.childImageSharp.fluid} alt="meetings"/>
                    }
                    {
                        active === 3 && <Image fluid={liveTracking.childImageSharp.fluid} alt="load-management"/>
                    }
                    {
                        active === 4 && <Image fluid={liveTracking.childImageSharp.fluid} alt="services"/>
                    }
                </div>
                <button className={`${styles.tab_button + " button__get_started"}`}
                        id="express_get_started"
                        onClick={() => {
                            logger();
                            t.locale === 'ro' ? window.open('https://share-eu1.hsforms.com/1QBYJOzIWRNCOOQomhqKBGAfv7ye') :
                                window.open('https://share-eu1.hsforms.com/1s_SB9dP2Qjm6ZHHiTE7U3Qfv7ye')
                        }}>
                    {t.formatMessage({id: "common.getStartedNow"})}</button>
            </div>
        </div>
    )
}

export default Express;
