import React from "react";
import * as styles from './Future.module.scss';
import {useIntl} from "gatsby-plugin-intl";
import useWindowSize from "../../../hooks/useWindowSize";
import {graphql, useStaticQuery} from 'gatsby';
import Image from "gatsby-image";

export const getScreens = graphql`
  {
    screens: file(relativePath: { eq: "home/future/future-screens.webp" }) {
      childImageSharp { 
        fluid(quality: 100, webpQuality: 100, maxWidth: 3080) {
          ...GatsbyImageSharpFluid_noBase64
        }
      }
    }
    mobileScreens: file(relativePath: { eq: "home/future/future-screens-mobile.webp" }) {
      childImageSharp { 
        fluid(quality: 100, webpQuality: 100, maxWidth: 1080) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

 const Future = () => {
     const t = useIntl();
     const size = useWindowSize();
     const {
         screens,
         mobileScreens
     } = useStaticQuery(getScreens)

     return (
         <>
             <div className={styles.future_container} id="future">
                 {
                     <h2 className="page_title_section">{t.formatMessage({id: "future.title"})}</h2>
                 }
                 <h3 className="page_subtitle_section">{t.formatMessage({id: "future.subtitle"})}</h3>
             </div>
             <div className={styles.future_images}>
                 {
                     size.width > 768 ? <Image fluid={screens.childImageSharp.fluid} alt="transportation-industry"/> :
                         <Image fluid={mobileScreens.childImageSharp.fluid} alt="transportation-industry"/>
                 }
             </div>
         </>
     )
 }

 export default Future;
