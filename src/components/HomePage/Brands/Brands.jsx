import React from 'react';
import * as styles from "./Brands.module.scss";
import {useIntl} from "gatsby-plugin-intl";
import jurnalul from '../../../images/home/brands/jurnalulLogo.svg';
import proTV from '../../../images/home/brands/protvLogo.svg';
import businessMagazin from '../../../images/home/brands/businessLogo.svg';
import startUp from '../../../images/home/brands/startupLogo.svg';
import forbes from '../../../images/home/brands/forbesLogo.svg';
import iLikeIt from '../../../images/home/brands/iLikeItLogo.svg';
import agerpresLogo from '../../../images/home/brands/agerpresLogo.svg';

const Brands = () => {
    const t = useIntl();

    return (
        <div className={styles.brands_container}>
            <h3 className="page_subtitle_section">{t.formatMessage({id: "brands.title"})}</h3>
            <div className={styles.brands_logo}>
                <div className={styles.flexAuto}>
                    <img src={jurnalul} alt="jurnalul-ro"/>
                </div>
                <div className={styles.flexAuto}>
                    <img src={proTV} alt="protv"/>
                </div>
                <div className={styles.flexAuto}>
                    <img src={businessMagazin} alt="business-magazin"/>
                </div>
                <div className={styles.flexAuto}>
                    <img src={startUp} alt="startUp"/>
                </div>
                <div className={styles.flexAuto}>
                    <img src={forbes} alt="forbes"/>
                </div>
                <div className={styles.flexAuto}>
                    <img src={iLikeIt} alt="iLikeIt"/>
                </div>
                <div className={styles.flexAuto}>
                    <img src={agerpresLogo} alt="agerpres"/>
                </div>
            </div>
        </div>
    )

}

export default Brands;
