import React, {useEffect, useState} from 'react';
import * as styles from './Footer.module.scss';
import {changeLocale, useIntl} from "gatsby-plugin-intl";
import {withPrefix} from 'gatsby-link';
import {Link} from 'gatsby';
import useWindowSize from "../../hooks/useWindowSize";
import {useLocation} from "@reach/router";
import yloadLogoFooter from '../../images/logo-yload-global.svg';
import yloadWhiteLogoFooter from '../../images/footer/yload-logo-footer.svg';
import iconFacebook from '../../images/footer/facebook-logo.svg';
import iconFacebookWhite from '../../images/footer/icon-facebook-square.svg';
import iconInstagram from '../../images/footer/instagram-logo.svg';
import iconInstagramWhite from '../../images/footer/icon-instagram.svg';
import iconYoutube from '../../images/footer/youtube-logo.svg';
import iconYoutubeWhite from '../../images/footer/icon-youtube-square.svg';
import iconLinkedin from '../../images/footer/linkedin-logo.svg';
import iconLinkedinWhite from '../../images/footer/icon-linkedin.svg';
import googlePlayImg from '../../images/android-app-logos2.webp';
import appStoreImg from '../../images/apple-app-logos.webp';

const Footer = ({isDarkFooter}) => {
    const t = useIntl();
    const size = useWindowSize();
    const {pathname} = useLocation();
    const [openMenu, setOpenMenu] = useState(false);
    const [language, setLanguage] = useState('English');

    useEffect(() => {
        if (pathname.includes('/en/')) {
            return setLanguage('English');
        }
        if (pathname.includes('/ro/')) {
            return setLanguage('Romana');
        }
        if (pathname.includes('/de/')) {
            return setLanguage('Deutsch');
        }
        if (pathname.includes('/pl/')) {
            return setLanguage('Polish');
        }
    }, [])

    const onChangeLanguage = (lang) => {
        changeLocale(lang);
    }

    const linkWithPrefix = withPrefix(`${t.locale}/`);

    const textColor = {
        color: isDarkFooter ? '#FFF' : '#000'
    }

    return (
        <div className={styles.footer} style={isDarkFooter && {backgroundColor: '#1C1B1B'}}>
            <div className={styles.footer_container}>
                <div className={styles.footer_col + " " + styles.flex_auto} style={textColor}>
                    <img src={isDarkFooter ? yloadWhiteLogoFooter : yloadLogoFooter} alt="yload-logo"/>
                    {
                        size.width <= 768 && <div className={styles.divider}/>
                    }
                    <div className={styles.icons_row}>
                        <a href="https://m.facebook.com/yloadeu/" target="_blank" rel="noreferrer">
                            <img src={isDarkFooter ? iconFacebookWhite : iconFacebook} alt="facebook"/>
                        </a>
                        <a href="https://www.instagram.com/yload_/" target="_blank" rel="noreferrer">
                            <img src={isDarkFooter ? iconInstagramWhite : iconInstagram} alt="instagram"/>
                        </a>
                        <a href="https://www.youtube.com/channel/UCV5aJQgZ8NZNC0ypmfASCtg" target="_blank"
                           rel="noreferrer">
                            <img src={isDarkFooter ? iconYoutubeWhite : iconYoutube} alt="youtube"/>
                        </a>
                        <a href="https://www.linkedin.com/company/yload/" target="_blank">
                            <img src={isDarkFooter ? iconLinkedinWhite : iconLinkedin} alt="linkedin"/>
                        </a>
                    </div>
                    <div className={`${styles.select_container}` + " select_container"}>
                        <input value={'Language: ' + language} onClick={() => setOpenMenu(!openMenu)} readOnly
                               className="selectLanguage"
                               style={isDarkFooter && {color: '#FFFFFF', border: '1px solid #FFF'}}/>
                        {
                            openMenu && <ul>
                                <li onClick={() => {
                                    onChangeLanguage('en')
                                    setOpenMenu(false)
                                }}>English
                                </li>
                                <li onClick={() => {
                                    onChangeLanguage('ro')
                                    setOpenMenu(false)
                                }}>Romana
                                </li>
                                <li onClick={() => {
                                    onChangeLanguage('de')
                                    setOpenMenu(false)
                                }}>Deutsch
                                </li>
                                <li onClick={() => {
                                    onChangeLanguage('pl')
                                    setOpenMenu(false)
                                }}>Polish
                                </li>
                            </ul>
                        }
                    </div>
                    <div className={styles.download_section}>
                        <h5>{t.formatMessage({id: "common.downloadApp"})}</h5>
                        <div style={{display: "flex", flexWrap: 'nowrap'}}>
                            <a style={{width: 'min-content'}}
                               href='https://play.google.com/store/apps/details?id=org.yload.manager&hl=en&gl=US'
                               target='_blank' rel='noreferrer'>
                                <img src={googlePlayImg} alt="google-play"/>
                            </a>
                            <a style={{width: 'min-content'}} href='https://apps.apple.com/ro/app/yload/id1571229937'
                               target='_blank' rel='noreferrer'>
                                <img src={appStoreImg} alt="app-store"/>
                            </a>
                        </div>
                        {/*<hr style={{width: '100%'}}/>*/}
                        {/*<div style={{display: "flex", flexWrap: 'nowrap'}}>*/}
                        {/*    <a style={{width: 'min-content', textDecoration: 'none', color: '#000000'}} href='https://anpc.ro/ce-este-sal/'*/}
                        {/*       target='_blank' rel='noreferrer'>*/}
                        {/*        <h5> ANPC </h5>*/}
                        {/*    </a>*/}
                        {/*    <a style={{width: 'min-content', textDecoration: 'none', color: '#000000'}} href='https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home2.show&lng=RO'*/}
                        {/*       target='_blank' rel='noreferrer'>*/}
                        {/*        <h5> SOL </h5>*/}
                        {/*    </a>*/}
                        {/*</div>*/}
                    </div>
                </div>

                <div className={styles.footer_col + " " + styles.flex_auto} style={textColor}>
                    <h4>{t.formatMessage({id: "footer.product.title"})}</h4>
                    <div className={styles.footer_items}>
                        <Link to={`/${t.locale}`}
                              style={textColor}>{t.formatMessage({id: "footer.product.productOverview"})}</Link>
                        {/*<a>{t.formatMessage({ id: "footer.product.pricing"})}</a>*/}
                        {/*<a>{t.formatMessage({ id: "footer.product.enterprise"})}</a>*/}
                        {/*<a>{t.formatMessage({ id: "footer.product.business"})}</a>*/}
                        <Link to={linkWithPrefix + "support"}
                              style={textColor}>{t.formatMessage({id: "footer.product.support"})}</Link>
                        <Link to={linkWithPrefix + "contact"}
                              style={textColor}>{t.formatMessage({id: "footer.product.contact"})}</Link>
                        <Link to={linkWithPrefix + "blog"}
                              style={textColor}>Blog</Link>
                        <Link to={linkWithPrefix + "privacy-policy"}
                              style={textColor}>{t.formatMessage({id: "footer.product.privacyPolicy"})}</Link>
                    </div>
                </div>

                <div className={styles.footer_col + " " + styles.flex_auto} style={textColor}>
                    <h4>{t.formatMessage({id: "footer.divisions.title"})}</h4>
                    <div className={styles.footer_items}>
                        <a href="https://yload.ro" target="_blank" rel="noreferrer" style={textColor}>Yload.ro</a>
                        <a href="https://trucks.yload.eu" target="_blank" rel="noreferrer"
                           style={textColor}>YTrucks</a>
                        <a href="https://network.yload.eu" target="_blank" rel="noreferrer"
                           style={textColor}>YNetwork</a>
                        {/*<a>Yload Express</a>*/}
                    </div>
                </div>

                <div className={styles.footer_col + " " + styles.flex_auto} style={textColor}>
                    <h4>{t.formatMessage({id: "footer.company.title"})}</h4>
                    <div className={styles.footer_items}>
                        <Link to={linkWithPrefix + "about-us"}
                              style={textColor}>{t.formatMessage({id: "footer.company.aboutUs"})}</Link>
                        <Link to={linkWithPrefix + "become-a-partner"}
                              style={textColor}>{t.formatMessage({id: "footer.company.becomePartner"})}</Link>
                        {/*<a>{t.formatMessage({ id: "footer.company.affiliates"})}</a>*/}
                        {/*<a>{t.formatMessage({ id: "footer.company.pressRoom"})}</a>*/}
                        {/*<Link to={linkWithPrefix + "investor-relations"}*/}
                        {/*      style={textColor}>{t.formatMessage({ id: "footer.company.investor"})}</Link>*/}
                        {/*<a>{t.formatMessage({ id: "footer.company.contactSales"})}</a>*/}
                        <Link to={linkWithPrefix + "broker-partnership"}
                              style={textColor}>{t.formatMessage({id: "footer.company.broker"})}</Link>
                        <Link to={linkWithPrefix + "career"}
                              style={textColor}>{t.formatMessage({id: "footer.company.careers"})}</Link>
                        <hr style={{width: '50%', margin: '15px auto 0'}}/>
                        <a href="https://anpc.ro/ce-este-sal/" target='_blank' rel="noreferrer"
                           style={{color: isDarkFooter ? '#FFF' : '#000', marginTop: '10px'}}>ANPC</a>
                        <a href="https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home2.show&lng=RO"
                           target='_blank' rel="noreferrer" style={{color: isDarkFooter ? '#FFF' : '#000'}}>SOL</a>
                    </div>
                </div>
            </div>
            <div className={styles.footer_copyright} style={isDarkFooter && {backgroundColor: '#1C1B1B'}}>
                Copyrights - Yload Global SA - Europe, Cluj Napoca, Romania © {new Date().getFullYear()}
            </div>
        </div>
    )
}

export default Footer;
