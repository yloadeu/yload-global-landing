import React, {useEffect, useState} from "react";
import * as styles from './MobileNavbar.module.scss';
import {changeLocale, useIntl} from "gatsby-plugin-intl";
import {Link} from 'gatsby';
import {useLocation} from "@reach/router";
import {withPrefix} from "gatsby-link";
import googlePlayImg from "../../../images/android-app-logos2.webp";
import appStoreImg from "../../../images/apple-app-logos.webp";
import enFlag from "../../../images/flags/en.svg";
import roFlag from "../../../images/flags/ro.svg";
import deFlag from "../../../images/flags/de.svg";
import plFlag from "../../../images/flags/pl.svg";

const MobileNavbar = ({openMenu, handleCloseMenu}) => {

    const t = useIntl();
    const {pathname} = useLocation();
    const [openSelection, setOpenSelection] = useState(false);
    const [language, setLanguage] = useState('English');
    const [flag, setFlag] = useState(enFlag);

    const onChangeLanguage = (lang) => {
        changeLocale(lang);
    }

    useEffect(() => {
        if (pathname.includes('/en/')) {
            setLanguage('English');
            setFlag(enFlag);
        }
        if (pathname.includes('/ro/')) {
            setLanguage('Romana');
            setFlag(roFlag);
        }
        if (pathname.includes('/de/')) {
            setLanguage('Deutsch');
            setFlag(deFlag);
        }
        if (pathname.includes('/pl/')) {
            setLanguage('Polish');
            setFlag(plFlag);
        }
    }, [])

    const linkWithPrefix = withPrefix(`${t.locale}/`);

    return (
        <div className={openMenu ? `${styles.mobile_navbar_container} ${styles.display}`
            : `${styles.mobile_navbar_container} ${styles.display_none}`}>
            <div>
                <ul className={styles.menu_links}>
                    <a href={linkWithPrefix + '#product'} id="navbar_product"
                       onClick={handleCloseMenu}>{t.formatMessage({id: "navbar.product"})}</a>
                    <a style={{color: ''}} href={linkWithPrefix + 'broker-partnership'} id="navbar_broker"
                       onClick={handleCloseMenu}>{t.formatMessage({id: "navbar.broker"})}</a>
                    <a href={linkWithPrefix + 'about-us'} id="navbar_about"
                       onClick={handleCloseMenu}>{t.formatMessage({id: "navbar.aboutUs"})}</a>
                    <a href={linkWithPrefix + 'support'} id="navbar_support"
                       onClick={handleCloseMenu}>{t.formatMessage({id: "navbar.support"})}</a>
                    <a href={linkWithPrefix + 'career'} id="navbar_career"
                       onClick={handleCloseMenu}>{t.formatMessage({id: "navbar.careers"})}</a>
                    <a href={linkWithPrefix + 'contact'} id="navbar_contact"
                       onClick={handleCloseMenu}>{t.formatMessage({id: "navbar.contact"})}</a>
                    <a href={linkWithPrefix + 'blog'} id="navbar_blog"
                       onClick={handleCloseMenu}>Blog</a>
                </ul>
                <div style={{display: "flex", flexDirection: "column"}}>
                    <div className={styles.buttons}>
                        <a className="button"
                              id="trucks_redirect_button"
                              href="https://trucks.yload.eu"
                              target="_blank"
                        >
                            {t.formatMessage({id: "navbar.yloadTruckers"})}
                        </a>
                        <a className="button"
                              id="network_redirect_button"
                              href="https://network.yload.eu/"
                              target="_blank"
                        >
                            {t.formatMessage({id: "navbar.yloadNetwork"})}
                        </a>
                    </div>
                    <div className={`${styles.select_container}` + " select_container"}>
                        <div onClick={() => setOpenSelection(!openSelection)}
                             className={`${styles.selectLanguage}` + " selectLanguage"}>
                            <img src={flag} alt=""/>
                            {language}
                        </div>
                        {
                            openSelection && <ul>
                                <li onClick={() => {
                                    onChangeLanguage('en')
                                    setOpenSelection(false)
                                }}><img src={enFlag} alt=""/> English
                                </li>
                                <li onClick={() => {
                                    onChangeLanguage('ro')
                                    setOpenSelection(false)
                                }}><img src={roFlag} alt=""/> Romana
                                </li>
                                <li onClick={() => {
                                    onChangeLanguage('de')
                                    setOpenSelection(false)
                                }}><img src={deFlag} alt=""/> Deutsch
                                </li>
                                <li onClick={() => {
                                    onChangeLanguage('pl')
                                    setOpenSelection(false)
                                }}><img src={plFlag} alt="" style={{width: '24px', height: '14px'}}/> Polish
                                </li>
                            </ul>
                        }
                    </div>
                    <div className={styles.download_section}>
                        <h5>{t.formatMessage({id: "common.downloadApp"})}</h5>
                        <a href='https://play.google.com/store/apps/details?id=org.yload.manager&hl=en&gl=US'
                           target='_blank' rel='noreferrer'>
                            <img src={googlePlayImg} alt="google-play"/>
                        </a>
                        <a href='https://apps.apple.com/ro/app/yload/id1571229937'
                           target='_blank' rel='noreferrer'>
                            <img src={appStoreImg} alt="app-store"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default MobileNavbar;
