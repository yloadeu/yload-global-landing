import React, {useState} from 'react';
import {useIntl} from 'gatsby-plugin-intl';
import useWindowSize from '../../hooks/useWindowSize';
import useScroll from '../../hooks/useScroll';
import {withPrefix} from 'gatsby-link';
import {navigate} from 'gatsby';
import { Turn as Hamburger } from 'hamburger-react';
import MobileNavbar from "./MobileNavbar/MobileNavbar";
import siteLogo from "../../images/logo-yload-global.svg";
import siteMobileLogo from "../../images/logo-yload-global-mobile.svg";

const Navbar = ({navbarClass}) => {
    const t = useIntl();
    const size = useWindowSize();
    const {scrollDirection} = useScroll();
    const [openMenu, setOpenMenu] = useState(false);

    const handleCloseMenu = () => {
        setOpenMenu(false);
    }

    const linkWithPrefix = withPrefix(`${t.locale}/`);

    const navStyles = {
        active: {
            visibility: "visible",
            transition: "all 0.5s"
        },
        hidden: {
            visibility: "hidden",
            transition: "all 0.5s",
            transform: "translateY(-100%)"
        }
    }

    return (
        <div className={`${navbarClass} navbar`}
             style={scrollDirection === "down" ? navStyles.active : navStyles.hidden}>
            <div className="navbar_container">
                <div className="logo_container">
                    {
                        size.width > 601 && <img src={siteLogo} alt="site_logo" id="navbar_image_1"
                                                 onClick={() => window.location.assign(linkWithPrefix)} style={{cursor: "pointer"}}/>
                    }
                    {
                        size.width <= 601 && <img src={siteMobileLogo} alt="site_logo" id="navbar_image_1"
                                                  onClick={() => window.location.assign(linkWithPrefix)} style={{cursor: "pointer"}}/>
                    }
                </div>
                {
                    size.width > 1200 && <div className="menu_elements">
                        <ul>
                            <a href={linkWithPrefix + '#product'} id="navbar_product">{t.formatMessage({id: "navbar.product"})}</a>
                            <a href={linkWithPrefix + 'about-us'} id="navbar_about">{t.formatMessage({id: "navbar.aboutUs"})}</a>
                            <a href={linkWithPrefix + 'support'} id="navbar_support">{t.formatMessage({id: "navbar.support"})}</a>
                            <a href={linkWithPrefix + 'career'} id="navbar_career">{t.formatMessage({id: "navbar.careers"})}</a>
                            <a href={linkWithPrefix + 'contact'} id="navbar_contact">{t.formatMessage({id: "navbar.contact"})}</a>
                            <a href={linkWithPrefix + 'blog'} id="navbar_blog">Blog</a>
                            <a className="button button_broker"
                               id="navbar_broker"
                               href={linkWithPrefix + 'broker-partnership'}>
                                {t.formatMessage({id: "navbar.broker"})}
                            </a>
                            <a className="button button_logistics"
                                  id="trucks_redirect_button"
                                  href="https://trucks.yload.eu"
                                  target="_blank"
                            >
                                {t.formatMessage({id: "navbar.yloadTruckers"})}
                            </a>
                            <a className="button button_get_started"
                                  id="network_redirect_button"
                                  href="https://network.yload.eu/"
                                  target="_blank"
                            >
                                {t.formatMessage({id: "navbar.yloadNetwork"})}
                            </a>
                        </ul>
                    </div>
                }
                {
                    size.width <= 1200 && <>
                        <div>
                            <Hamburger  toggled={openMenu} toggle={setOpenMenu} direction="right" size={26} color="#000000"/>
                        </div>
                        <MobileNavbar openMenu={openMenu} handleCloseMenu={handleCloseMenu}/>
                    </>
                }
            </div>
        </div>
    )
}

export default Navbar;
