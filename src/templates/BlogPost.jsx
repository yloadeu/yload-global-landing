import React, {useEffect} from 'react'
import {graphql} from 'gatsby'
import SEO from "../components/SEOComponent/SEOComponent";
import Navbar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";
import Blog from "../components/Blog/Blog";

const BlogPostTemplate = (props) => {
    const post = props.data.contentfulBlogPage;
    return (
        <div className="layout_container">
            <SEO/>
            <Navbar navbarClass="partner_navbar"/>
            <Blog post={post}/>
            <Footer isDarkFooter={true}/>
        </div>
    )
}

export default BlogPostTemplate

export const pageQuery = graphql`
      query BlogPostBySlug(
        $slug: String!
      ) {
        contentfulBlogPage(slug: { eq: $slug }) {
          slug
          title
          heroImage {
            gatsbyImage(placeholder: BLURRED, width: 3000, quality: 100)
            file {
                url
            }
          }
          description {
            description
          }
          content {
            raw
            references {
                ... on ContentfulAsset {
                contentful_id
                __typename
                file {
                    url
                }
              }
              gatsbyImage(placeholder: BLURRED, width: 3000, quality: 100)
            }
          }
          author {
            avatar {
              file {
                url
              }
            }
            name
            role
          }
          readingTime
          publishDate(formatString: "DD MMM YYYY")
          tags
          relatedArticles {
            title
            heroImage {
              file {
                url
              }
            }
            description {
              description
            }
            slug
          }
        }
      }
    `