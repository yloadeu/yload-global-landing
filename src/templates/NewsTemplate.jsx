import React from 'react'
import {graphql} from 'gatsby'
import SEO from "../components/SEOComponent/SEOComponent";
import Navbar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";
import News from "../components/Investors/News/News";

const NewsTemplate = (props) => {
    const news = props.data.contentfulNews;
    return (
        <div className="layout_container">
            <SEO/>
            <Navbar navbarClass="partner_navbar"/>
            <News data={news} />
            <Footer isDarkFooter={true}/>
        </div>
    )
}

export default NewsTemplate

export const pageQuery = graphql`
      query NewsBySlug(
        $slug: String!
      ) {
        contentfulNews(slug: { eq: $slug }) {
            date(formatString: "DD MMM YYYY")
            slug
            description {
              description
            }
            content {
              raw
            }
            title
        }
      }
    `